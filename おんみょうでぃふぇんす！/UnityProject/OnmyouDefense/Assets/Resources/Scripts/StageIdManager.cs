using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//*****************************************************************
//
//  各シーンからステージIDにアクセスできるクラス
//
//*****************************************************************
public class StageIdManager : MonoBehaviour
{
    // 選択されたステージID
    private int _SelectStageID;
    // ロードをかけるステージのファイルパス
    private string _StageFilePath;


    void Start()
    {

    }

    //  シングルトンなインスタンス
    public static StageIdManager Instance
    {
        get; private set;
    }

    private void Awake()
    {
        _SelectStageID = -1;
        _StageFilePath = "";

        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    //  ステージIDのプロパティ
    public void SetStageID(int id)
    {
        _SelectStageID = id;

        Debug.Log(_SelectStageID);
    }

    public int GetStageID()
    {
        return _SelectStageID;
    }

    //  ステージファイルパスのプロパティ
    public void SetLoadStagePath(string path)
    {
        _StageFilePath = path;

        Debug.Log(_StageFilePath);
    }

    public string GetLoadStagePath()
    {
        return _StageFilePath;
    }
}
