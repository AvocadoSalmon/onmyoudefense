using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagePanel : MonoBehaviour
{
    private Popup _popup;
    
    void Start()
    {
        _popup = this.GetComponent<Popup>();
    }

    //  有効化された時
    private void OnEnable()
    {
        _popup = this.GetComponent<Popup>();
        _popup.OnOpen();
    }

    
    //  無効化された時
    private void OnDisable()
    {
        _popup.OnClose();
    }
}
