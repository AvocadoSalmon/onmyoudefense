using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Text
using System;
using UnityEngine.Networking;
using MiniJSON; // Json
using System.IO; // File


/// <summary>
/// Manager main.
/// </summary>
public class TitleManager : MonoBehaviour
{
    [SerializeField]private Text _checkName;
    [SerializeField]private Text _inputName;
    [SerializeField]private Text _Message;

    [SerializeField]private GameObject _InputNamePanelObj = default;
    [SerializeField]private GameObject _CheckNamePanelObj = default;
    [SerializeField]private GameObject _MessagePanelObj = default;

    [SerializeField]private EaseIn _EaseInImage = default;
    [SerializeField]private TitleLogoTween _LogoTween = default;
    [SerializeField]private FadeIO _FadeIO = default;
    [SerializeField] private GameObject _MarkObj = default;

    private string accessPath = "";     //LocalSaveJson
    private bool _checkSave;
    private string _jsonName = "";
    private string _databaseName = "";
    private int _databaseClearNum = -1;


    public static string _keyName = "Name";
    public static string _keyClearNum = "ClearNum";

    private void Awake()
    {
        //  ウィンドウサイズを固定する
        Screen.SetResolution(1280, 720, false);
    }

        private void Start()
    {
        //*******************************************************
        //  ONLY DEBUG
        //********************************************************
#if UNITY_EDITOR
        //  デバッグ用ローカルファイル全削除
        //PlayerPrefs.DeleteAll();
#endif

        //  タイトルフロー開始
        StartCoroutine(TitleFlow());

    }

    //------------------------------------------------------------------
    //  名前チェックする
    //------------------------------------------------------------------
    private IEnumerator CheckName()
    {
        yield return StartCoroutine(GetJsonFromWww());   //  データベースからの名前を保存
        yield return StartCoroutine(LoadJsonFromLocal());// ローカルからの名前を保存

        //メッセージパネルを有効化
        SetPanelActive("CheckName",false);
        SetPanelActive("Message",true);

        Debug.Log("json :" + _jsonName);
        Debug.Log("database :" + _databaseName);

        //  名前を比較
        if(string.Compare(_jsonName,_databaseName) == 0)
        {   //  一致したらシーン共通変数に格納ステージセレクトへ
             _Message.text = "照合に成功しました";

            StageSelectManager.SetPlayerName(_databaseName);
            StageSelectManager.SetStageClearNum(_databaseClearNum);

            //  ステージセレクトへ遷移
            ChangeScene.Execute("SceneStageSelect");
        }
        else
        {
            _Message.text = "照合に失敗しました(error:Name)";

            Debug.Log("２つの名前が一致しません\nゲームを終了します");

            //  2秒後にゲームを閉じる
            Invoke(nameof(Quit), 2.0f);
        }
    }

    //--------------------------------------------------------------
    //  ゲーム終了処理
    //--------------------------------------------------------------
    private void Quit()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_STANDALONE
            UnityEngine.Application.Quit();
        #endif
    }

    /// <summary>
    /// Raises the click clear display event.
    /// </summary>
    public void OnClickClearDisplay()
    {
        _checkName.text = "";
        _inputName.text = "";
        _Message.text = "";
    }

    /// <summary>
    /// Raises the click get json from www event.
    /// </summary>
    public void OnClickGetMessagesApi()
    {
        _checkName.text = "プレイヤーネームをデータベースと照合中...";
        GetJsonFromWww();
    }

    /// <summary>
    /// Raises the click get json from www event.
    /// </summary>
    public void OnClickSetMessageApi()
    {
        _checkName.text = "プレイヤーネームをデータベースと照合中...";
        CreateJsonToWww();
    }

    /// <summary>
    /// Gets the json from www.
    /// </summary>
    private IEnumerator GetJsonFromWww()
    {
        // APIが設置してあるURLパス
        const string url = "http://localhost/NameMatchingSystem/onmyou/getMessage";

        // Wwwを利用して json データ取得をリクエストする
        yield return StartCoroutine(GetMessages(url, CallbackWwwSuccess, CallbackWwwFailed));
    }

    /// <summary>
    /// Callbacks the www success.
    /// </summary>
    /// <param name="response">Response.</param>
    private void CallbackWwwSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        List<UserData> messageList = UserDataModel.DeserializeFromJson(response);

        //  最初のレコードの値をコピー
        _databaseName = messageList[0]._Name;
        _databaseClearNum = messageList[0]._ClearNum;

        string sStrOutput = "";
        foreach (UserData message in messageList)
        {
            sStrOutput += $"Name:{message._Name}\n";
            sStrOutput += $"ClearNum:{message._ClearNum}\n";
            sStrOutput += $"Date:{message._Date}\n";
        }

        //Debug.Log(sStrOutput);
    }

    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWwwFailed()
    {
        // jsonデータ取得に失敗した
        _Message.text = "jsonデータ取得に失敗しました";
    }

    /// <summary>
    /// Callbacks the API success.
    /// </summary>
    /// <param name="response">Response.</param>
    private void CallbackApiSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        _Message.text = response;
    }

    /// <summary>
    /// Downloads the json.
    /// </summary>
    /// <returns>The json.</returns>
    /// <param name="url">S tgt UR.</param>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    private IEnumerator GetMessages(string url, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        // WWWを利用してリクエストを送る
        var webRequest = UnityWebRequest.Get(url);

        //タイムアウトの指定
        webRequest.timeout = 10;

        // WWWレスポンス待ち
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    /// <summary>
    /// Response the check for time out WWW.
    /// </summary>
    /// <returns>The check for time out WWW.</returns>
    /// <param name="webRequest">Www.</param>
    /// <param name="timeout">Timeout.</param>
    private IEnumerator ResponseCheckForTimeOutWWW(UnityWebRequest webRequest, float timeout)
    {
        float requestTime = Time.time;

        while (!webRequest.isDone)
        {
            if (Time.time - requestTime < timeout)
            {
                yield return null;
            }
            else
            {
                Debug.LogWarning("TimeOut"); //タイムアウト
                break;
            }
        }

        yield return null;
    }


    /// <summary>
    /// Sets the json to www.
    /// </summary>
    private void CreateJsonToWww()
    {
        // APIが設置してあるURLパス
        string sTgtURL = "http://localhost/NameMatchingSystem/onmyou/createMessage";

        string name = _inputName.text;
        int clearNum = StageSelectManager.GetStageClearNum();

        // Wwwを利用して json データ設定をリクエストする
        StartCoroutine(SetMessage(sTgtURL, name, clearNum, CallbackApiSuccess, CallbackWwwFailed));
    }

    /// <summary>
    /// Sets the message.
    /// </summary>
    /// <returns>The message.</returns>
    /// <param name="url">URL target.</param>
    /// <param name="name">Name.</param>
    /// <param name="message">Message.</param>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    private IEnumerator SetMessage(string url, string name, int clearNum, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("Name", name);
        form.AddField("ClearNum", clearNum);

        // WWWを利用してリクエストを送る
        var webRequest = UnityWebRequest.Post(url, form);

        //タイムアウトの指定
        webRequest.timeout = 10;

        // WWWレスポンス待ち
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            Debug.Log("データベースに登録しました！");
            if (null != cbkSuccess)
            {
                cbkSuccess("データベースに登録しました！");

                //  登録完了したらステージセレクトへ
                ChangeScene.Execute("SceneStageSelect");
            }
        }
    }

    //------------------------------------------------------------------
    //  各パネルの状態の切り替え
    //------------------------------------------------------------------
    private void SetPanelActive(string panelName, bool active)
    {
        if("InputName" == panelName)
        {
            if( null != _InputNamePanelObj )
            {
                _InputNamePanelObj.SetActive(active);
            }
            else Debug.Log("_InputNamePanelObjが無い");
        }
        else if("CheckName" == panelName)
        {
            if( null != _CheckNamePanelObj )
            {
                _CheckNamePanelObj.SetActive(active);
            }
            else Debug.Log("_CheckNamePanelObjが無い");
        }
        else if("Message" == panelName)
        {
            if( null != _MessagePanelObj )
            {
                _MessagePanelObj.SetActive(active);
            }
            else Debug.Log("_MessagePanelObjが無い");
        }
        else Debug.Log("引数のパネル名が間違っています");
    }

    //------------------------------------------------------------------
    //  ローカルJSONを読み込んで変数に保存
    //------------------------------------------------------------------
    private IEnumerator LoadJsonFromLocal()
    {
        // jsonファイルを読み込む
        string str = "";

        str = PlayerPrefs.GetString ("Name", "");

        //  読み込めたら変数に保存
        _jsonName = str;

        yield return null;
    }

    //------------------------------------------------------------------
    //  プレイヤーデータをローカルに保存
    //------------------------------------------------------------------
    public void SavePlayerData()
    {
        //  キーを設定してセーブ
        PlayerPrefs.SetString (_keyName, _inputName.text);
        PlayerPrefs.SetInt (_keyClearNum, 0);
        PlayerPrefs.Save ();

        //  値が入っていればセーブ完了
        if( PlayerPrefs.HasKey (_keyName) && 
           PlayerPrefs.HasKey  (_keyClearNum) )
        {
            _Message.text = "セーブが完了しました！";
        }
        else
        {
            _Message.text = "セーブに失敗しました";
        }
    }

    //------------------------------------------------------------------
    //  名前入力後OKボタンを押した時の処理
    //------------------------------------------------------------------
    public void OnClickOKButton()
    {
        //プレイヤーデータをローカルに保存
        SavePlayerData();

        //名前入力パネルOFF
        SetPanelActive("InputName",false);

        //メッセージパネルON
        SetPanelActive("Message",true);

        //ここより先は保存できている場合

        StageSelectManager.SetPlayerName(_inputName.text);  //シーン共通の変数に保存
        StageSelectManager.SetStageClearNum(0);             //シーン共通の変数に保存

        CreateJsonToWww(); //レコードを新規作成してデータベースに保存
    }

    //------------------------------------------------------------------
    //  セーブデータがあるかどうかチェック
    //------------------------------------------------------------------
    private bool CheckSaveData()
    {
        //  値が入っていればセーブ完了
        if( PlayerPrefs.HasKey (_keyName) && 
           PlayerPrefs.HasKey  (_keyClearNum) )
        {
            return true;
        }

        return false;
    }

    //------------------------------------------------------------------
    //  チェックシーケンス前のアニメーション
    //------------------------------------------------------------------
    private IEnumerator UIAnimation()
    {
        //  立ち絵スライドイン
        yield return StartCoroutine(_EaseInImage.StartAnimation());

        //  タイトルロゴアニメ開始
        yield return  StartCoroutine(_LogoTween.StartAnimation());

        //  フェードアウト・イン
        yield return StartCoroutine(_FadeIO.StartAnimation());

        //  陰陽マーク表示
        _MarkObj.SetActive(true);
    }

    //------------------------------------------------------------------
    //  チェックシーケンス
    //------------------------------------------------------------------
    private IEnumerator Check()
    {
        //あればチェックシーケンス、なければ名前入力シーケンスへ
        _checkSave = CheckSaveData();

        if (_checkSave) // 名前チェック
        {
            SetPanelActive("InputName", false);
            SetPanelActive("CheckName", true);
            SetPanelActive("Message", false);

            StartCoroutine(CheckName()); // チェック開始
        }
        else // 名前入力
        {
            //  初回起動なので初回プレイフラグをON
            PlayerPrefsBool.SetBool("isPlayingFirst", true);

            SetPanelActive("InputName", true);
            SetPanelActive("CheckName", false);
            SetPanelActive("Message", false);
        }

        yield return null;
    }

    //------------------------------------------------------------------
    //  タイトルループ
    //------------------------------------------------------------------
    private IEnumerator TitleFlow()
    {
        //  UIアニメ開始
        yield return StartCoroutine(UIAnimation());

        //  チェックシーケンスへ
        yield return StartCoroutine(Check());
    }



}