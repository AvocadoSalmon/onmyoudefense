public class UserData
{
    public string _Name { get; set; }
    public int _ClearNum { get; set; }
    public string _Date { get; set; }

    public UserData()
    {
        _Name = "";
        _ClearNum = 0;
        _Date = "";
    }
}