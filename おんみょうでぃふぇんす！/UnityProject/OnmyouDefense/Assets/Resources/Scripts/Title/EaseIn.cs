using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EaseIn : MonoBehaviour
{
    RectTransform _RectTransform;

    //  TweenSE
    [SerializeField] private AudioClip _TweenClip = default;

    //  オーディオソース
    private AudioSource _AudioSource;

    void Start()
    {
        //  コンポーネントを取得
        _RectTransform = GetComponent<RectTransform>();
        _AudioSource = GetComponent<AudioSource>();
    }

    //--------------------------------------------------
    //  アニメーション再生
    //--------------------------------------------------
    public IEnumerator StartAnimation()
    {
        bool complete = false;

        this.enabled = true;

        //  スライドイン
        _RectTransform.DOLocalMoveX(450f, 0.5f)
         .SetEase(Ease.OutBounce)
         .OnStart(() => {
             //  SEを再生
             _AudioSource.clip = _TweenClip;
             _AudioSource.Play(); ;
         })
         .OnComplete(() =>
         {

 
             complete = true;
         });

        //  完了まで待つ
        yield return new WaitUntil(() => complete == true);
    }
}
