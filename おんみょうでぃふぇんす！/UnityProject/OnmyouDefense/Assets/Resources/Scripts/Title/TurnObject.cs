using UnityEngine;
using DG.Tweening;

public class TurnObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.DOLocalRotate(new Vector3(0,0,-720),1.0f,RotateMode.FastBeyond360)
            .SetEase(Ease.InOutQuad)
            .SetLoops(-1,LoopType.Incremental);
    }

    void OnDestroy ()
    {
        //�j��
        Destroy (gameObject);
    }
}
