using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeIO : MonoBehaviour
{
    //  TweenSE
    [SerializeField] private AudioClip _TweenClip = default;

    //  オーディオソース
    private AudioSource _AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        //  コンポーネントを取得
        _AudioSource = GetComponent<AudioSource>();
    }

    public IEnumerator StartAnimation()
    {
        bool complete = false;

        //  アニメーション準備
        Image fadeImage = GetComponent<Image>();
        fadeImage.enabled = true;
        Color c = fadeImage.color;
        c.a = 1.0f; // 初期値
        fadeImage.color = c;

        Sequence sequence = DOTween.Sequence();

        sequence.Append
            (
                    DOTween.ToAlpha
                    (
                        () => fadeImage.color,
                        color => fadeImage.color = color,
                        1f, // 目標値
                        0.5f // 所要時間
                    )
                    .OnStart(() => {

                    })
            );
        sequence.Append
            (
                DOTween.ToAlpha
                (
                    () => fadeImage.color,
                    color => fadeImage.color = color,
                    0f, // 目標値
                    0.5f // 所要時間
                )
            )
            .OnStart(
            () =>
            {
                if (_TweenClip != null)
                { 
                    //  SEを再生
                    _AudioSource.clip = _TweenClip;
                    _AudioSource.Play(); ;
                }
            })
            .OnComplete(
            () =>
            {
                complete = true;
            });

        //  完了まで待つ
        yield return new WaitUntil(() => complete == true);
    }
}
