using System.Collections;
using System;
using UnityEngine;


//*****************************************************************
//
//  シーンを切り替える
//
//*****************************************************************
public static class ChangeScene
{

    public static void Execute(string sceneName)
    {
        const float delay = 1.5f;       //  実行までにかかる秒数
        const float interval = 1.0f;    //  暗転にかかる秒数
        
        //StartCoroutineが使えないのでCoroutineHandlerを実行
        CoroutineHandler.StartStaticCoroutine
        (
            FadeManager.Instance.DelayLoadScene
            (
                delay,
                sceneName,
                interval
            )
        );
    }
}