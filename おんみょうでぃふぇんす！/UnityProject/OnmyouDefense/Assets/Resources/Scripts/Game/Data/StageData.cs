//*************************************************************************
//
//  ステージID毎のデータクラス
//
//*************************************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StageData : MonoBehaviour
{
    //  敵ベースの体力
    public float _BaseHP { get; set; }
    //  敵がポップする間隔
    public float _PopInterval { get; set; }
    //  敵ベース情報構造体
    public struct EnemyBaseParam
    {
        public float _baseHP { get; set; } 
        public float _popINterval { get; set; }

        public EnemyBaseParam(float hp, float interval)
        {
            _baseHP = hp;
            _popINterval = interval;
        }
    }
    StageData.EnemyBaseParam[] _EnemyBasePrm =
        new StageData.EnemyBaseParam[(int)GameManager.StageID.STAGE_MAX]
        {
            new StageData.EnemyBaseParam(20,   5),  //  Stage01
            new StageData.EnemyBaseParam(50,   5),  //  Stage02
            new StageData.EnemyBaseParam(100,  3)   //  Stage03
        };
    //  敵の出現確率
    public float[] _SpawnRate { get; set; }

    //  敵の出現確率構造体
    public struct EnemySpawnRate
    {
        // 敵毎の出現率 
        public float[] _spawnRate { get; set; }

        public EnemySpawnRate(float hiykoRate, float hitsujiRate, float penginRate, float moguraRate, float oniRate)
        {
            _spawnRate = new float[(int)GameManager.EnemyType.MAX];

            _spawnRate[(int)GameManager.EnemyType.HIYOKO] = hiykoRate;
            _spawnRate[(int)GameManager.EnemyType.HITSUJI] = hitsujiRate;
            _spawnRate[(int)GameManager.EnemyType.PENGIN] = penginRate;
            _spawnRate[(int)GameManager.EnemyType.MOGURA] = moguraRate;
            _spawnRate[(int)GameManager.EnemyType.ONI] = oniRate;
        }
    }
    //  敵出現率のパラメータ設定
    StageData.EnemySpawnRate[] _EnemySpawnRate =
        new StageData.EnemySpawnRate[(int)GameManager.StageID.STAGE_MAX]
        {
            new StageData.EnemySpawnRate(100f,100f,30f,0f,0f), //STAGE_01
            new StageData.EnemySpawnRate(0f,100f,100f,20f,0f), //STAGE_02
            new StageData.EnemySpawnRate(100f,0f,0f,70f,30f),  //STAGE_03
        };

    public StageData(int stageId )
    {
        _BaseHP = _EnemyBasePrm[stageId]._baseHP;
        _PopInterval = _EnemyBasePrm[stageId]._popINterval;

        _SpawnRate = new float[(int)GameManager.EnemyType.MAX];
        for (int i=0;i<(int)GameManager.EnemyType.MAX;i++)
        {
            _SpawnRate[i] = _EnemySpawnRate[stageId]._spawnRate[i];
        }
    }
}
