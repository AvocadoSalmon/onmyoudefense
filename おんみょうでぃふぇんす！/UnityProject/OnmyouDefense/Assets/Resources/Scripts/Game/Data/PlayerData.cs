//*************************************************************************
//
//  プレイヤーのタイプ毎のデータクラス
//
//*************************************************************************
public class PlayerData
{
    public float _Cost { get; set; }    //  召喚コスト
    public float _Speed { get; set; }   //  スピード
    public float _Health { get; set; }  //  体力

    //  基本パラメータ構造体
    public struct BasicParam
    {
        public float _cost { get; set; }    //  召喚コスト
        public float _speed { get; set; }   //  スピード
        public float _health { get; set; }  //  体力

        public BasicParam(float cost, float speed, float health)
        {
            _cost = cost;
            _speed = speed;
            _health = health;
        }
    }

    public float _Angle { get; set; } //  回転する角度
    public float _Time { get; set; }   //  回転にかかる時間

    //  移動アニメーションのパラメータ構造体
    public struct AnimeParam
    {
        public float _angle { get; set; } //  回転する角度
        public float _time{ get; set; }   //  回転にかかる時間

        public AnimeParam(float angle, float time)
        {
            _angle = angle;
            _time = time;
        }
    }

    public float _Damage { get; set; }     //  与えるダメージ
    public float _Interval { get; set; }   //  攻撃間隔

    //  攻撃のパラメータ構造体
    public struct AttackParam
    {
        public float _damage { get; set; }     //  与えるダメージ
        public float _interval { get; set; }   //  攻撃の間隔(秒)

        public AttackParam(float damage, float interval)
        {
            _damage = damage;
            _interval = interval;
        }
    }

    //  プレイヤーの基本パラメータ設定
    PlayerData.BasicParam[] _Basic =
        new PlayerData.BasicParam[(int)GameManager.PlayerType.MAX]
        {
            new PlayerData.BasicParam( 3f, 0.2f,  5f), // TSURU,
            new PlayerData.BasicParam(10f, 1.5f,  3f), // NEZUMI,
            new PlayerData.BasicParam(20f, 0.5f, 10f), // SARU,
            new PlayerData.BasicParam(30f, 1.0f, 15f), // INU,
            new PlayerData.BasicParam(50f, 0.5f, 50f)  // TORA,
        };

    //  プレイヤーの移動アニメパラメータ設定
    PlayerData.AnimeParam[] _Anime =
        new PlayerData.AnimeParam[(int)GameManager.PlayerType.MAX]
        {
            new PlayerData.AnimeParam( 0f, 0.0f),  // TSURU,
            new PlayerData.AnimeParam(10f, 0.05f), // NEZUMI,
            new PlayerData.AnimeParam(10f, 0.2f),  // SARU,
            new PlayerData.AnimeParam(20f, 0.1f),  // INU,
            new PlayerData.AnimeParam(20f, 0.4f)   // TORA,
        };

    //  プレイヤーの攻撃パラメータ設定
    PlayerData.AttackParam[] _Attack =
        new PlayerData.AttackParam[(int)GameManager.PlayerType.MAX]
        {
            new PlayerData.AttackParam(1.0f, 1.0f),  // TSURU,
            new PlayerData.AttackParam(0.2f, 1.0f),  // NEZUMI,
            new PlayerData.AttackParam(  5f, 0.5f),  // SARU,
            new PlayerData.AttackParam(  8f, 1.0f),  // INU,
            new PlayerData.AttackParam( 30f, 2.0f)   // TORA,
        };

    public PlayerData(int playerType)
    {
        _Cost   = _Basic[playerType]._cost;
        _Speed  = _Basic[playerType]._speed;
        _Health = _Basic[playerType]._health;

        _Angle = _Anime[playerType]._angle;
        _Time  = _Anime[playerType]._time;

        _Damage   = _Attack[playerType]._damage;
        _Interval = _Attack[playerType]._interval;
    }
}
