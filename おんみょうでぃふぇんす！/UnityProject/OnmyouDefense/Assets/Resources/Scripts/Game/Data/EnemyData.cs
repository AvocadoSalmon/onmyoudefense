//*************************************************************************
//
//  プレイヤーのタイプ毎のデータクラス
//
//*************************************************************************
public class EnemyData
{   public float _Speed { get; set; }   //  スピード
    public float _Health { get; set; }  //  体力

    //  基本パラメータ構造体
    public struct BasicParam
    {
        public float _speed { get; set; }   //  スピード
        public float _health { get; set; }  //  体力

        public BasicParam(float speed, float health)
        {
            _speed = speed;
            _health = health;
        }
    }

    public float _Angle { get; set; } //  回転する角度
    public float _Time { get; set; }   //  回転にかかる時間

    //  移動アニメーションのパラメータ構造体
    public struct AnimeParam
    {
        public float _angle { get; set; } //  回転する角度
        public float _time { get; set; }   //  回転にかかる時間

        public AnimeParam(float angle, float time)
        {
            _angle = angle;
            _time = time;
        }
    }

    public float _Damage { get; set; }     //  与えるダメージ
    public float _Interval { get; set; }   //  攻撃間隔

    //  攻撃のパラメータ構造体
    public struct AttackParam
    {
        public float _damage { get; set; }     //  与えるダメージ
        public float _interval { get; set; }   //  攻撃の間隔(秒)

        public AttackParam(float damage, float interval)
        {
            _damage = damage;
            _interval = interval;
        }
    }

    //  敵の基本パラメータ設定
    EnemyData.BasicParam[] _Basic =
        new EnemyData.BasicParam[(int)GameManager.EnemyType.MAX]
        {
            new EnemyData.BasicParam(0.2f,  5f), // HIYOKO
            new EnemyData.BasicParam(0.2f,  3f), // HITSUJI
            new EnemyData.BasicParam(0.3f, 10f), // PENGIN
            new EnemyData.BasicParam(0.2f, 15f), // MOGURA
            new EnemyData.BasicParam(0.3f, 100f) // ONI   
        };

    //  敵の移動アニメパラメータ設定
    EnemyData.AnimeParam[] _Anime =
        new EnemyData.AnimeParam[(int)GameManager.EnemyType.MAX]
        {
            new EnemyData.AnimeParam(10f, 0.05f), // HIYOKO
            new EnemyData.AnimeParam(10f, 0.1f),  // HITSUJI
            new EnemyData.AnimeParam(10f, 0.1f),  // PENGIN
            new EnemyData.AnimeParam(10f, 0.2f),  // MOGURA
            new EnemyData.AnimeParam(10f, 0.4f)   // ONI   
        };

    //  敵の攻撃パラメータ設定
    EnemyData.AttackParam[] _Attack =
        new EnemyData.AttackParam[(int)GameManager.EnemyType.MAX]
        {
            new EnemyData.AttackParam(0.3f, 2.0f),  // HIYOKO
            new EnemyData.AttackParam(0.5f, 1.0f),  // HITSUJI
            new EnemyData.AttackParam(  3f, 3.5f),  // PENGIN
            new EnemyData.AttackParam(  5f, 0.5f),  // MOGURA
            new EnemyData.AttackParam( 20f, 3.0f)   // ONI   
        };

    public EnemyData(int enemyType)
    {
        _Speed = _Basic[enemyType]._speed;
        _Health = _Basic[enemyType]._health;

        _Angle = _Anime[enemyType]._angle;
        _Time = _Anime[enemyType]._time;

        _Damage = _Attack[enemyType]._damage;
        _Interval = _Attack[enemyType]._interval;
    }
}