using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//*****************************************************************
//
//  敵攻撃クラス
//
//*****************************************************************
public class EnemyAttack : MonoBehaviour
{
    private bool _isCompleted;                  //  攻撃完了フラグ
    private bool _canAttack;                    //  攻撃が可能かどうか
    private bool _HasShooted;                   //  攻撃を発射したかどうか
    private Tween _AttackTween;                 //  攻撃アニメのTween
    protected Rigidbody _Rigidbody;             // キャラクターのRigidbody
    private float _CurrentLaunchForce = 30f;    //  弾の発射力
    private string _AttackerTag;                //  攻撃主のタグ

    public Rigidbody _Bullet;                   //  弾のリジッドボディ


    //-----------------------------------------------------------------------
    //  初期設定
    //-----------------------------------------------------------------------
    private void Awake()
    {
        _isCompleted = false;
        _canAttack = true;
        _HasShooted = false;
        _AttackerTag = "";
    }
    void Start()
    {
        _Rigidbody = this.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _isCompleted = false;
        _canAttack = true;
        _HasShooted = false;
        _AttackerTag = "";
    }

    //-----------------------------------------------------------------------
    //  攻撃する
    //-----------------------------------------------------------------------
    public void Attack(EnemyData enemyData)
    {
        if (_canAttack) //  攻撃可能状態なら
        {
            if (!_HasShooted)   //  攻撃を未発射なら
            {
                Fire(enemyData);
            }
            else SetAttackAnimation(enemyData); //  攻撃アニメーション
        }
    }

    //-----------------------------------------------------------------------
    //  攻撃アニメーションの設定
    //-----------------------------------------------------------------------
    private void SetAttackAnimation(EnemyData enemyData)
    {
        if (this._AttackTween == null)
        {
            //モデルにモーションがないので回転を攻撃モーションとする
            this._AttackTween = this.transform.DORotate
                                     (
                                        new Vector3(0, 360, 0),
                                        1.0f,
                                        RotateMode.FastBeyond360
                                     )
            .SetEase(Ease.OutQuad)
            .OnComplete(() =>
            {
                //   interval後にリセット
                Invoke("ResetAfterComplete", enemyData._Interval);
            });
        }
    }

    //-----------------------------------------------------------------------
    //  攻撃（見えない弾を撃って攻撃ということにする）
    //-----------------------------------------------------------------------
    private void Fire(EnemyData enemyData)
    {
        //  発射済みにする
        _HasShooted = true;

        //  インスタンス生成とリジッドボディ化を同時に行う
        Rigidbody bulletInstance =
            Instantiate(
                         _Bullet,
                         new Vector3(
                                        transform.position.x,
                                        transform.position.y + 0.5f, //少し上
                                        transform.position.z),
                         transform.rotation) as Rigidbody;

        //  ダメージ設定
        BulletEffect bulletEff = bulletInstance.GetComponent<BulletEffect>();
        bulletEff.SetAttackPower(enemyData._Damage);
        bulletEff.SetAttackCharacterTag(_AttackerTag);

        //  発射速度を設定 
        bulletInstance.velocity = _CurrentLaunchForce * transform.forward;
    }

    //-----------------------------------------------------------------------
    //  isCompletedのプロパティ
    //-----------------------------------------------------------------------
    public void SetIsCompleted(bool complete)
    {
        if (_isCompleted != complete)
        {
            _isCompleted = complete;

            //Debug.Log("_isCompletedが " + _isCompleted + " にセットされました");
        }
    }

    public bool GetIsCompleted()
    {
        return _isCompleted;
    }

    //-----------------------------------------------------------------------
    //  _canAttackのプロパティ
    //-----------------------------------------------------------------------
    public void SetcanAttack(bool canAttack)
    {
        if (_canAttack != canAttack)
        {
            _canAttack = canAttack;

            //Debug.Log("_canAttackが " + _canAttack + " にセットされました");
        }
    }

    public bool GetcanAttack()
    {
        return _canAttack;
    }

    //-----------------------------------------------------------------------
    //  DOTWEENNを停止
    //-----------------------------------------------------------------------
    private void Stop()
    {
        // 今の_Tweenはキャンセルし
        this._AttackTween.Kill();
        this._AttackTween = null;

        //  強制敵に無回転にする
        this._Rigidbody.rotation = Quaternion.identity;
    }

    //-----------------------------------------------------------------------
    //  interval秒後にリセット処理する
    //-----------------------------------------------------------------------
    private void ResetAfterComplete()
    {
        _isCompleted = true;   //  完了フラグをON

        if (this._AttackTween != null) Stop();   // Tweenを停止
        _HasShooted = false;                     // 未発射にリセット
    }

    //-----------------------------------------------------------------------
    //  攻撃側のオブジェクトタグを設定する※生成時に呼ぶ
    //-----------------------------------------------------------------------
    public void SetAttackerTagName(string tag)
    {
        //  弾が発射されるまで保存用
        _AttackerTag = tag;
    }

}
