using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//*****************************************************************
//
//  プレイヤー攻撃クラス
//
//*****************************************************************
public class PlayerAttack : MonoBehaviour
{
    private bool _IsCompleted;                  //  攻撃アニメ完了フラグ
    private bool _CanAttack;                    //  攻撃が可能かどうか
    private bool _HasShooted;                   //  攻撃を発射したかどうか
    private Tween _AttackTween;                 //  攻撃アニメのTween
    private Rigidbody _Rigidbody;               // キャラクターのRigidbody
    private float _CurrentLaunchForce = 30f;    //  弾の発射力
    private string _AttackerTag;                //  攻撃主のタグ


    public Rigidbody _Bullet;                   //  弾のリジッドボディ

    //-----------------------------------------------------------------------
    //  初期設定
    //-----------------------------------------------------------------------
    private void Awake()
    {
        _IsCompleted = false;
        _CanAttack = true;
        _HasShooted = false;
        _AttackerTag = "";
    }
    void Start()
    {
        _Rigidbody = this.GetComponent<Rigidbody>();

    }

    private void OnEnable()
    {
        _IsCompleted = false;
        _CanAttack = true;
        _HasShooted = false;
        _AttackerTag = "";
    }

    //-----------------------------------------------------------------------
    //  攻撃する
    //-----------------------------------------------------------------------
    public void Attack(PlayerData playerData)
    {
        if (_CanAttack)         //  攻撃可能状態で
        {
            if (!_HasShooted)   //  攻撃を未発射なら
            {
                Fire(playerData);
            }
            else SetAttackAnimation(playerData); //  攻撃アニメーション
        }
    }

    //-----------------------------------------------------------------------
    //  攻撃アニメーションの設定
    //-----------------------------------------------------------------------
    private void SetAttackAnimation(PlayerData playerData)
    {
        if (this._AttackTween == null)
        { 
            //モデルにモーションがないので回転を攻撃モーションとする
            this._AttackTween = this.transform.DORotate
                                     (
                                        new Vector3(0, 360, 0),
                                        1.0f,
                                        RotateMode.FastBeyond360
                                     )
            .SetEase(Ease.OutQuad)
            .OnComplete(() =>
           {
               //   interval後にリセット
               Invoke("ResetAfterComplete", playerData._Interval);
            });
         }
    }

    //-----------------------------------------------------------------------
    //  攻撃（見えない弾を撃って攻撃ということにする）
    //-----------------------------------------------------------------------
    private void Fire(PlayerData playerData)
    {
        //  発射済みにする
        _HasShooted = true;

        //  インスタンス生成とリジッドボディ化を同時に行-う
        Rigidbody bulletInstance = 
            Instantiate(
                         _Bullet,
                         new Vector3(
                                        transform.position.x,
                                        transform.position.y + 0.5f, //少し上
                                        transform.position.z),
                         transform.rotation) as Rigidbody;

        //  ダメージ設定
        BulletEffect bulletEff = bulletInstance.GetComponent<BulletEffect>();
        bulletEff.SetAttackPower(playerData._Damage);
        bulletEff.SetAttackCharacterTag(_AttackerTag);

        //  発射速度を設定 
        bulletInstance.velocity = _CurrentLaunchForce * -transform.forward;
    }

    //-----------------------------------------------------------------------
    //  isCompletedのプロパティ
    //-----------------------------------------------------------------------
    public void SetIsCompleted(bool complete)
    {
        if (_IsCompleted != complete)
        {
            _IsCompleted = complete;

            //Debug.Log("_IsCompletedが " + _IsCompleted + " にセットされました");
        }
    }

    public bool GetIsCompleted()
    {
        return _IsCompleted;
    }

    //-----------------------------------------------------------------------
    //  _CanAttackのプロパティ
    //-----------------------------------------------------------------------
    public void SetcanAttack(bool canAttack)
    {
        if (_CanAttack != canAttack)
        {
            _CanAttack = canAttack;

            //Debug.Log("_CanAttackが " + _CanAttack + " にセットされました");
        }
    }

    public bool GetcanAttack()
    {
        return _CanAttack;
    }

    //-----------------------------------------------------------------------
    //  DOTWEENNを停止
    //-----------------------------------------------------------------------
    private void Stop()
    {
        // 今の_Tweenはキャンセルし
        this._AttackTween.Kill();
        this._AttackTween = null;

        //  強制敵に無回転にする
        this._Rigidbody.rotation = Quaternion.identity;
    }

    //-----------------------------------------------------------------------
    //  interval秒後にリセット処理する
    //-----------------------------------------------------------------------
    private void ResetAfterComplete()
    {
        _IsCompleted = true;   //  完了フラグをON

        if (this._AttackTween != null) Stop();   // Tweenを停止
        _HasShooted = false;                     // 未発射にリセット
    }

    //-----------------------------------------------------------------------
    //  攻撃側のオブジェクトタグを設定する※生成時に呼ぶ
    //-----------------------------------------------------------------------
    public void SetAttackerTagName(string tag)
    {
        //  弾が発射されるまで保存用
        _AttackerTag = tag;
    }


}