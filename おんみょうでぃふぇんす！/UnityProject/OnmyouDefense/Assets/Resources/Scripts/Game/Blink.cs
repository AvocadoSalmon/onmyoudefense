using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(CanvasGroup))]
public class Blink : MonoBehaviour
{
    public float DurationSeconds;
    public Ease EaseType;

    private CanvasGroup canvasGroup;

    void Start()
    {
        this.canvasGroup = this.GetComponent<CanvasGroup>();
        
    }

    public IEnumerator Blinking()
    {
        bool complete = false;

        //  ImageコンポーネントをONにする
        this.GetComponent<Image>().enabled = true;

        //  3回繰り返す
        this.canvasGroup.DOFade(0.0f, this.DurationSeconds).SetEase(this.EaseType).SetLoops(3, LoopType.Yoyo)
                            .OnComplete(() =>
                            {
                                complete = true;
                            });

        //  完了まで待つ
        yield return new WaitUntil(() => complete == true);

        //  オブジェクトを削除
        Destroy(this.gameObject);
    }
}