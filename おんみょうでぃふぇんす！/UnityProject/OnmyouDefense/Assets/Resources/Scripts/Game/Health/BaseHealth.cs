using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BaseHealth : MonoBehaviour
{
    [SerializeField] private Slider _Slider;                          //  体力バーのSliderクラス
    [SerializeField] private Image _FillImage;                        //  体力バーの画像
    [SerializeField] private GameObject _HitPrefab;                   //  ヒットエフェクトのプレハブ
    [SerializeField] private GameObject _BurnPrefab;                  //  破壊エフェクトのプレハブ
    [SerializeField] private Color _FullHealthColor = Color.green;    //  体力バーの初期色
    [SerializeField] private Color _ZeroHealthColor = Color.red;      //  体力バーの危険域色
    [SerializeField] private AudioClip _BurnClip = default;           //  破壊SEクリップ

    private float _StartingHealth = 100f;           //  最大体力
    private float _CurrentHealth;                   //  現在体力
    private bool _CanDamaged;                       //  被ダメージ可能フラグ
    private bool _Dead;                             //  死亡フラグ
    private bool _IsDeadStagingEnd;                 //  死亡演出が終わったかどうか
    private ParticleSystem _HitParticles;           //  ヒットエフェクト
    private ParticleSystem _BurnParticles;          //  破壊エフェクト
    private AudioSource[] _AudioSource;             //  オーディオソース
    private AudioSource _HitAudio;                  //  ヒットのオーディオソース  
    private AudioSource _BurnAudio;                 //  破壊のオーディオソース

    private GameManager.CharacterType _Type;        //  キャラタイプ
    private float _DestroyWithEffectTIme = 3.0f;    //  ベース破壊時のタイマーs

    private void Awake()
    {
        _IsDeadStagingEnd = false;
        _Dead = false;
        _CanDamaged = true;

        _AudioSource = GameObject.Find("GameManager").GetComponents<AudioSource>();

        _HitAudio = _AudioSource[(int)GameManager.AudioType.SFX_DAMAGE];
        _BurnAudio = _AudioSource[(int)GameManager.AudioType.SFX];

        //  爆発パーティクルの生成とオーディオのコンポーネント取得
        _HitParticles = Instantiate(_HitPrefab).GetComponent<ParticleSystem>();
        _HitParticles.transform.parent = this.transform;
        _HitParticles.gameObject.SetActive(false);   //  パーティクルを無効化

        //  破壊パーティクルの生成とオーディオのコンポーネント取得
        _BurnParticles = Instantiate(_BurnPrefab).GetComponent<ParticleSystem>();
        _BurnParticles.gameObject.SetActive(false);   //  パーティクルを無効化
        


    }

    void Start()
    {

    }

    private void OnEnable()
    {
        _CurrentHealth = _StartingHealth;
        _Slider.maxValue = _StartingHealth;
        _Dead = false;
        _IsDeadStagingEnd = false;
        _CanDamaged = true;



        SetHealthUI();

    }

    //-------------------------------------------------------------------------
    //  被ダメージ処理
    //-------------------------------------------------------------------------
    public void TakeDamage(float amount)
    {
        if (!_CanDamaged) return;   //  被ダメージ不可なら無視

        //  ヒットパーティクルをベースの位置で有効化
        _HitParticles.transform.position = transform.position;
        _HitParticles.gameObject.SetActive(true);

        _HitParticles.Play();    //  パーティクルを再生
        _HitAudio.Play();        // ヒット音を再生

        _CurrentHealth -= amount;   //  ダメージを受ける

        SetHealthUI();  //  体力を更新

        // 生存中に死亡演出が未完了で体力が0になったら
        if (_CurrentHealth <= 0f && !_Dead )
        {
            _CurrentHealth = 0;
            _CanDamaged = false;
            OnDeath();  //  死亡！
        }
    }


    private void SetHealthUI()
    {
        // スライダーの値を設定
        _Slider.value = _CurrentHealth;

        // スライダーの値によって色を変える
        _FillImage.color = Color.Lerp(_ZeroHealthColor, _FullHealthColor, _CurrentHealth / _StartingHealth);
    }


    private void OnDeath()
    {
        //  ヒットパーティクルを削除
        Destroy(_HitParticles.gameObject);
        //  破壊パーティクルを削除
        //Destroy(_BurnParticles.gameObject);

        //  ベースを3秒後に非表示（エフェクトと合わせる）
        DOVirtual.DelayedCall(_DestroyWithEffectTIme, () =>
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        });
        
        //  死亡フラグON
        _Dead = true;
    }

    //-----------------------------------------------------------------------
    //  現在のHPを返す
    //-----------------------------------------------------------------------
    public float GetHP()
    {
        return _CurrentHealth;
    }

    //-----------------------------------------------------------------------
    //  最大HPを設定する
    //-----------------------------------------------------------------------
    public void SetMaxHP(float maxHealth)
    {
        if (_StartingHealth != maxHealth)
        {

            _StartingHealth = maxHealth;
            _CurrentHealth = maxHealth;
            _Slider.maxValue = maxHealth;

            Debug.Log("ベースの最大HPが" + _StartingHealth + "に設定されました");
        }
    }

    //-----------------------------------------------------------------------
    //  死亡しているかどうかを返す
    //-----------------------------------------------------------------------
    public bool IsDead()
    {
        return _Dead;
    }

    //-----------------------------------------------------------------------
    //  死亡フラグを設定
    //-----------------------------------------------------------------------
    public void SetIsDead(bool dead)
    {
        if (_Dead != dead)
        {
            Debug.Log("_Deadが" + dead + "に設定されました");

            _Dead = dead;
        }
    }

    //-----------------------------------------------------------------------
    //  死亡演出再生
    //-----------------------------------------------------------------------
    public void DeadStaging()
    {
        //----------------------------------------
        //  死亡演出処理
        //----------------------------------------

        //  破壊エフェクト再生
        PlayParticle();



    }

    //-----------------------------------------------------------------------
    //  破壊エフェクト再生
    //-----------------------------------------------------------------------
    private void PlayParticle()
    {
        //  破壊パーティクルをベースの位置で有効化
        _BurnParticles.transform.position = new Vector3(
                                                        transform.position.x,
                                                        transform.position.y,
                                                        transform.position.z
                                                       ) ;
        _BurnParticles.gameObject.SetActive(true);
        _BurnParticles.Play();

        //  破壊音を3秒後に再生
        DOVirtual.DelayedCall(_DestroyWithEffectTIme, () =>
        {
            // 破壊音を再生
            _BurnAudio.clip = _BurnClip;
            _BurnAudio.Play();

            //  ベースを描画しない
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;

            //  破壊エフェクトの終了後の処理
            StartCoroutine(ParticleWorking());
        });        
    }


    //-----------------------------------------------------------------------
    //  破壊エフェクトの終了後の処理
    //-----------------------------------------------------------------------
    IEnumerator ParticleWorking()
    {
        yield return new WaitWhile(() => _BurnParticles.IsAlive(true));

        //  演出終了
        _IsDeadStagingEnd = true;
    }

    //-----------------------------------------------------------------------
    //  _Typeのプロパティ
    //-----------------------------------------------------------------------
    public void SetCharaType(GameManager.CharacterType type)
    {
        _Type = type;
    }
    public GameManager.CharacterType GetCharaType()
    {
        return _Type;
    }

    //-----------------------------------------------------------------------
    //  ベースが死亡したかどうかのゲッター
    //-----------------------------------------------------------------------
    public bool GetIsDead()
    {
        return _Dead;
    }

    //-----------------------------------------------------------------------
    //  死亡演出終了したかどうかのゲッター
    //-----------------------------------------------------------------------
    public bool GetIsDeadStagingEnd()
    {
        return _IsDeadStagingEnd;
    }
}
