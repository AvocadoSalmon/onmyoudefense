using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;


public class CharacterHealth : MonoBehaviour
{
    public Slider _Slider;                          //  体力バーのSliderクラス
    public Image _FillImage;                        //  体力バーの画像
    public GameObject _HitPrefab;                   //  ヒットエフェクトのプレハブ

    [SerializeField] private AudioSource[] _HitAudio; //  ヒットSE

    private float _StartingHealth = 100f;           //  最大体力
    private Color _FullHealthColor = Color.green;   //  体力バーの初期色
    private Color _ZeroHealthColor = Color.red;     //  体力バーの危険域色
    private float _CurrentHealth;                   //  現在体力
    private bool  _Dead;                            //  死亡フラグ
    private ParticleSystem _HitParticles;           //  ヒットエフェクト
    private Tween _DeadTween;                       //  死亡アニメのTween
    private bool _IsDeadAnimEnd;                    //  死亡アニメが終わったかどうか
    private ParticleSystem _DeadEffect;             //  死亡エフェクト
    private GameObject _DeadEffectPrefab;           //  死亡エフェクトプレハブ


    private void Awake()
    {
        _IsDeadAnimEnd = false;

        //  爆発パーティクルの生成とオーディオのコンポーネント取得
        _HitParticles = Instantiate(_HitPrefab).GetComponent<ParticleSystem>();
        _HitParticles.transform.parent = this.transform;
        _HitAudio = GameObject.Find("GameManager").GetComponents<AudioSource>();
        _HitParticles.gameObject.SetActive(false);   //  パーティクルを無効化
    }
    private void Start()
    {
        
    }

    private void OnEnable()
    {
        _CurrentHealth = _StartingHealth;
        _Slider.maxValue = _StartingHealth;
        _Dead = false;
        _IsDeadAnimEnd = false;



        SetHealthUI();
    }

    private void Update()
    {
        if (_IsDeadAnimEnd)   //  死亡アニメ再生が終わったら
        {
            Destroy(_HitParticles.gameObject);  //  パーティクルを削除
            _Dead = true;  //  死亡フラグON
        }
    }

    //-------------------------------------------------------------------------
    //  被ダメージ処理
    //-------------------------------------------------------------------------
    public void TakeDamage(float amount)
    {
        //  爆発パーティクルをキャラの位置で有効化
        _HitParticles.transform.position = transform.position;
        _HitParticles.gameObject.SetActive(true);

        _HitParticles.Play();    //  パーティクルを再生
        _HitAudio[(int)GameManager.AudioType.SFX_DAMAGE].Play();        //  ヒット音を再生

        _CurrentHealth -= amount;   //  ダメージを受ける

        SetHealthUI();  //  体力を更新

        if (_CurrentHealth <= 0f && !_Dead) // 生存中に体力が0になったら
        {
            _CurrentHealth = 0;
            OnDeath();  //  死亡！
        }
    }


    private void SetHealthUI()
    {
        // スライダーの値を設定
        _Slider.value = _CurrentHealth;

        // スライダーの値によって色を変える
        _FillImage.color = Color.Lerp(_ZeroHealthColor, _FullHealthColor, _CurrentHealth / _StartingHealth);
    }


    private void OnDeath()
    {
        DeadAnimation();    //  死亡アニメ再生
    }

    //-----------------------------------------------------------------------
    //  現在のHPを返す
    //-----------------------------------------------------------------------
    public float GetHP()
    {
        return _CurrentHealth;
    }

    //-----------------------------------------------------------------------
    //  最大HPを設定する
    //-----------------------------------------------------------------------
    public void SetMaxHP(float maxHealth)
    {
        if (_StartingHealth != maxHealth)
        {
            
            _StartingHealth = maxHealth;
            _CurrentHealth = maxHealth;
            _Slider.maxValue = maxHealth;

            //Debug.Log("最大HPが" + _StartingHealth + "に設定されました");
        }
    }

    //-----------------------------------------------------------------------
    //  死亡しているかどうかを返す
    //-----------------------------------------------------------------------
    public bool IsDead()
    {
        return _Dead;
    }

    //-----------------------------------------------------------------------
    //  死亡フラグを設定
    //-----------------------------------------------------------------------
    public void SetIsDead(bool dead)
    {
        if (_Dead != dead)
        {
            Debug.Log("_Deadが" + dead + "に設定されました");

            _Dead = dead;
        }
    }

    //-----------------------------------------------------------------------
    //  死亡アニメーション再生
    //-----------------------------------------------------------------------
    private void DeadAnimation()
    {
        //  １回強制的に回転をリセット
        this.transform.rotation = Quaternion.identity;

        if (this._DeadTween == null)
        {
            //モデルにモーションがないので回転を攻撃モーションとする
            this._DeadTween = this.transform.DORotate
                                     (
                                        new Vector3(-90, 0, 0),
                                        1.5f
                                     )
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                //  死亡エフェクト再生
                StartCoroutine(PlayDeadEffect());

                _IsDeadAnimEnd = true;  //  死亡アニメ終了
                Stop();
            });
        }
    }

    //-----------------------------------------------------------------------
    //  DOTWEENNを停止
    //-----------------------------------------------------------------------
    private void Stop()
    {
        // 今の_Tweenはキャンセルし
        this._DeadTween.Kill();
        this._DeadTween = null;

        //  強制敵に無回転にする
        this.transform.rotation = Quaternion.identity;
    }

    //-----------------------------------------------------------------------
    //  死亡エフェクトプレハブを設定
    //-----------------------------------------------------------------------
    public void SetDeadEffectPrefab(GameObject effect)
    {
        _DeadEffectPrefab = effect;
    }

    //-----------------------------------------------------------------------
    //  死亡エフェクト生成
    //-----------------------------------------------------------------------
    private IEnumerator PlayDeadEffect()
    {
        //  死亡エフェクト生成
        _DeadEffect = Instantiate(_DeadEffectPrefab).GetComponent<ParticleSystem>();
        _DeadEffect.transform.position = this.transform.position;

        yield return null;

    }


}