using UnityEngine;
using DG.Tweening;

public class Rotaion360 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.DOLocalRotate(new Vector3(0,0,-360f),10.0f,RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .SetLoops(-1,LoopType.Restart);
    }

    void OnDestroy ()
    {
        //�j��
        Destroy (gameObject);
    }
}
