using UnityEngine;

public class BulletEffect : MonoBehaviour
{

    public float _MaxLifeTime = 0.2f;

    private float _AttackPower;             //  キャラの攻撃力
    private string _AttakCharacterTag = ""; //  攻撃したキャラのタグ
    private bool _isFirst;                  //  連続で当たらないようにするフラグ

    //  衝突するオブジェクトのタイプ
    private enum CollisionType
    {
        OTHER = -1,
        PLAYER,
        PLAYER_BASE,
        ENEMY,
        ENEMY_BASE,

        MAX
    };

    

    private void Start()
    {
        _isFirst = true;

        //  弾の寿命は0.2秒
        Destroy(this.gameObject, 0.2f);
    }

    //--------------------------------------------------------------------------
    //  衝突判定
    //--------------------------------------------------------------------------
    private void OnTriggerEnter(Collider other)
    {
        CollisionType type = CollisionType.OTHER;

        if (other.gameObject.tag == "Player") type = CollisionType.PLAYER;
        else if (other.gameObject.tag == "PlayerBase") type = CollisionType.PLAYER_BASE;
        else if (other.gameObject.tag == "Enemy") type = CollisionType.ENEMY;
        else if (other.gameObject.tag == "EnemyBase") type = CollisionType.ENEMY_BASE;

        //  当たったのがキャラクターかベースじゃないならスキップ
        if (type == CollisionType.OTHER)
        {
            //  弾同士なら干渉しない
            if (other.gameObject.tag == "Bullet") return;

            Destroy(gameObject);  //  弾を削除
            return;
        }

        //  撃ったプレイヤーと同じタグならスキップ
        if (other.gameObject.tag == _AttakCharacterTag) return;
        
        //  初回じゃないならスキップ
        if (!_isFirst) return;

        //---------------------------------------
        //  ダメージを受ける(キャラ)
        //---------------------------------------
        if (type == CollisionType.PLAYER || type == CollisionType.ENEMY)
        {
            Rigidbody targetRigidbody = other.GetComponent<Rigidbody>();
            CharacterHealth targetHealth = targetRigidbody.GetComponent<CharacterHealth>();

            float damage = CalculateDamage(this._AttackPower);   //  ダメージを計算

            Debug.Log("攻撃 :" + _AttakCharacterTag);
            Debug.Log("ダメージ :" + other.gameObject.tag);

            targetHealth.TakeDamage(damage);         //  被弾した側がダメージ！
        }
        //---------------------------------------
        //  ダメージを受ける(ベース)
        //---------------------------------------
        else if (type == CollisionType.PLAYER_BASE || type == CollisionType.ENEMY_BASE)
        {
            BaseHealth targetHealth = other.GetComponent<BaseHealth>();
            float damage = CalculateDamage(this._AttackPower);   //  ダメージを計算

            targetHealth.TakeDamage(damage);         //  被弾した側がダメージ！
        }

        _isFirst = false;
        Destroy(gameObject);  //  弾を削除
    }

    //-----------------------------------------------------------------------
    //  与えるダメージを計算
    //-----------------------------------------------------------------------
    private float CalculateDamage(float damage)
    {
        // たまにクリティカルが出る
        float criticalRate = 10f;           //  クリティカル率(%)
        float criticalDamageRate = 1.5f;    //  クリティカルダメージ倍率

        float maxValue = 100f;
        criticalRate /= maxValue;
        float lottery = Random.Range(0f, maxValue);
        float rate = lottery / maxValue;


        float dealDamage = 0;
        if (rate > criticalRate) dealDamage = damage;
        else dealDamage = damage * criticalDamageRate;

        dealDamage = Mathf.Max(0f, dealDamage);   //  ダメージの値を0以上にする

        return dealDamage;
    }

    //-----------------------------------------------------------------------
    //  _AttackPowerのプロパティ
    //-----------------------------------------------------------------------
    public void SetAttackPower(float power)
    {
        _AttackPower = power;

        //Debug.Log("BulletEffect:_AttackPowerが " + _AttackPower + " にセットされました");
    }

    public float GetAttackPower()
    {
        return _AttackPower;
    }

    //-----------------------------------------------------------------------
    //  _AttakCharacterTagのプロパティ
    //-----------------------------------------------------------------------
    public void SetAttackCharacterTag(string tag)
    {
        if (_AttakCharacterTag != tag)
        {
            _AttakCharacterTag = tag;

            Debug.Log("_AttakCharacterTagが " + _AttakCharacterTag + " にセットされました");
        }
    }

    public string GetAttakCharacterTag()
    {
        return _AttakCharacterTag;
    }
}