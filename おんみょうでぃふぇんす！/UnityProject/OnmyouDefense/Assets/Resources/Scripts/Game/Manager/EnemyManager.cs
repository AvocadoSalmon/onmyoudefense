using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnemyState;
using UniRx;

//*****************************************************************
//
//  敵管理クラス
//
//*****************************************************************
public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject _DeadEffectPrefab = default;

    //  敵コンポーネント
    public CharacterHealth _Health;
    public EnemyMovement _Movement;
    public EnemyAttack _Attack;


    private int _EnemyType;                //  敵の種類
    private EnemyData[] _enemyDataList;    //  パラメータリスト

    //変更前のステート名
    private string _prevStateName;


    //ステート
    public StateProcessor StateProcessor { get; set; } = new StateProcessor();
    public CharacterStateIdle StateIdle { get; set; } = new CharacterStateIdle();
    public CharacterStateMove StateMove { get; set; } = new CharacterStateMove();
    public CharacterStateAttack StateAttack { get; set; } = new CharacterStateAttack();
    public CharacterStateDead StateDead { get; set; } = new CharacterStateDead();

    private void Awake()
    {
        _EnemyType = -1;

        //  各キャラのパラメータ設定
        _enemyDataList = new EnemyData[(int)GameManager.EnemyType.MAX]
        {
            new EnemyData((int)GameManager.EnemyType.HIYOKO ), // HIYOKO
            new EnemyData((int)GameManager.EnemyType.HITSUJI), // HITSUJI
            new EnemyData((int)GameManager.EnemyType.PENGIN),  // PENGIN
            new EnemyData((int)GameManager.EnemyType.MOGURA),  // MOGURA
            new EnemyData((int)GameManager.EnemyType.ONI)      // ONI   
        };

        //ステートの初期化
        StateProcessor.State.Value = StateIdle;
        StateIdle.ExecAction = Idle;
        StateMove.ExecAction = Move;
        StateAttack.ExecAction = Attack;
        StateDead.ExecAction = Dead;

    }

    void Start()
    {
        //  コンポーネントを取得
        _Health = this.GetComponent<CharacterHealth>();
        _Movement = this.GetComponent<EnemyMovement>();
        _Attack = this.GetComponent<EnemyAttack>();

        //ステートの値が変更されたら実行処理を行うようにする
        StateProcessor.State
            .Where(_ => StateProcessor.State.Value.GetStateName() != _prevStateName)
            .Subscribe(_ =>
            {
                //Debug.Log("Now State:" + StateProcessor.State.Value.GetStateName());
                _prevStateName = StateProcessor.State.Value.GetStateName();
                StateProcessor.Execute();
            })
            .AddTo(this);
    }

    public void Setup(int type)
    {
        //  敵タイプ別に初期値を設定
        _EnemyType = type;
        _Movement.SetSpeed(_enemyDataList[_EnemyType]._Speed);
        _Health.SetMaxHP(_enemyDataList[_EnemyType]._Health);
        _Health.SetDeadEffectPrefab(_DeadEffectPrefab);


        StateProcessor.State.Value = StateIdle;  //  最初は待機状態
        _Health.SetIsDead(false);                //  死亡フラグをfalseに
    }

    private void Update()
    {
        if (_Health.IsDead()) return;  //  死んでいるプレイヤーは処理しない

        //  ステートのUpdateを実行
        StateProcessor.State.Value.Update(this);
    }

    //-----------------------------------------------------------------------
    //  オブジェクトを消去
    //-----------------------------------------------------------------------
    public void DstroyObject()
    {
        _Health.SetIsDead(true);
    }

    //-----------------------------------------------------------------------
    //  コンポーネントを無効化
    //-----------------------------------------------------------------------
    public void DisableControl()
    {
        _Health.enabled = false;
        _Movement.enabled = false;
        _Attack.enabled = false;

        this.gameObject.SetActive(false);
    }

    //-----------------------------------------------------------------------
    //  コンポーネントを有効化
    //-----------------------------------------------------------------------
    public void EnableControl()
    {
        _Health.enabled = true;
        _Movement.enabled = true;
        _Attack.enabled = true;

        this.gameObject.SetActive(true);
    }

    //-----------------------------------------------------------------------
    //  リセット
    //-----------------------------------------------------------------------
    public void Reset()
    {
        this.gameObject.SetActive(false);
        this.gameObject.SetActive(true);
    }

    //-----------------------------------------------------------------------
    //  _Movementの移動先を設定
    //-----------------------------------------------------------------------
    public void SetMoveTarget(Vector3 target)
    {
        if (_Movement != null) _Movement.SetTargetPosition(target);
        else Debug.Log("_Movementがnull!");
    }

    //-----------------------------------------------------------------------
    //  _EnemyTypeのプロパティ
    //-----------------------------------------------------------------------
    public void SetEnemyType(int type)
    {
        if (_EnemyType != type)
        {
            _EnemyType = type;
            //Debug.Log("_EnemyTypeが " + _EnemyType + " にセットされました");
        }
    }

    public int GetEnemyType()
    {
        return _EnemyType;
    }

    //-----------------------------------------------------------------------
    //  各ステートに変わった瞬間の処理
    //-----------------------------------------------------------------------
    public void Idle()
    {
        //Debug.Log("StateがIdleに状態遷移しました。");

        _Attack.SetIsCompleted(false);  // 攻撃未完了にリセット
        _Attack.SetcanAttack(true);     // 攻撃可能

    }

    public void Move()
    {
        //Debug.Log("StateがMoveに状態遷移しました。");

        _Movement.SetCanMove(true);     //移動可能

        _Attack.SetIsCompleted(false);  // 攻撃未完了にリセット
        _Attack.SetcanAttack(true);     // 攻撃可能

    }

    public void Attack()
    {
        //Debug.Log("StateがAttackに状態遷移しました。");

    }

    public void Dead()
    {
        //Debug.Log("StateがDeadに状態遷移しました。");

    }

    //-----------------------------------------------------------------------
    //  待機:CharacterStateに渡すUpdate
    //-----------------------------------------------------------------------
    public void IdleUpdate()
    {
        _Movement.StopMoving(); //  移動停止

        //  移動可能ならMove状態へ
        if (_Movement.GetCanMove())
        {
            StateProcessor.State.Value = StateMove;
        }

        //  接敵していたら
        if (_Movement.GetHasEncounted())
        {
            StateProcessor.State.Value = StateAttack;   //攻撃中へ変更
        }
    }

    //-----------------------------------------------------------------------
    //  移動中:CharacterStateに渡すUpdate
    //-----------------------------------------------------------------------
    public void MoveUpdate()
    {
        if (
            _Movement.GetHasReached() ||  //  目的地に到達か
            _Movement.GetHasEncounted()   //  接敵で
           )
        {
            StateProcessor.State.Value = StateAttack;   //攻撃中へ変更
        }
        else _Movement.Movement(_enemyDataList[_EnemyType]); //  移動
    }

    //-----------------------------------------------------------------------
    //  攻撃中:CharacterStateに渡すUpdate
    //-----------------------------------------------------------------------
    public void AttackUpdate()
    {
        _Movement.StopMoving(); //  移動停止

        if (_Attack.GetIsCompleted())
        {
            _Attack.SetcanAttack(false);
            StateProcessor.State.Value = StateMove;
        }
        else _Attack.Attack(_enemyDataList[_EnemyType]);   //  攻撃
    }

    //-----------------------------------------------------------------------
    //  死亡中:CharacterStateに渡すUpdate
    //-----------------------------------------------------------------------
    public void DeadUpdate()
    {
        _Movement.StopMoving(); //  移動停止

    }

}

