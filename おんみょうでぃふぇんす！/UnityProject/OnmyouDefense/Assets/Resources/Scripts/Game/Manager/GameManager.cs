using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Assertions;
using DG.Tweening;
using UnityEngine.Networking;
using System;

public class GameManager : MonoBehaviour
{
    //  キャラの召喚位置
    [SerializeField] private GameObject[] _PlayerSpawnObject = default;
    [SerializeField] private GameObject[] _EnemySpawnObject = default;
    //  プレイヤーキャラのプレハブ
    [SerializeField] private GameObject[] _PlayerPrefabs = default;
    //  プレイヤーと敵のベース
    [SerializeField] private GameObject[] _Bases = default;
    //  各段階でのディレイ
    [SerializeField] private float _StartDelay = 0.0f;
    [SerializeField] private float _EndDelay = 1f;
    [SerializeField] private float _EndDelay2 = 0.5f;
    //  ゲーム中に表示するUIのGameObjectリスト
    [SerializeField] private GameObject _InGameUI = default;
    //  カメラオブジェクト
    [SerializeField] private GameObject _MainCamera = default;  //  メインカメラ
    [SerializeField] private GameObject _WinCamera = default;   //  勝利カメラ
    [SerializeField] private GameObject _LoseCamera = default;  //  敗北カメラ
    //  デバッグ用のFadeManagerのプレハブ
    [SerializeField] private GameObject _FadeManager = default;
    //  デバッグ用のStageIdManager
    [SerializeField] private GameObject _StageIdMnager = default;
    // Canvas
    [SerializeField] private GameObject _Canvas = default;
    // Tutorialオブジェクト
    [SerializeField] private GameObject _TutorialObject = default;
    //  メッセージパネルのプレハブ
    [SerializeField] private GameObject _MessagePanelPrefab = default;
    //  大メッセージパネルのプレハブ
    [SerializeField] private GameObject _TutorialMessagePanelPrefab = default;
    //  ゲーム結果表示用テキスト
    [SerializeField] private Text _ResultText = default;
    //  召喚カウントの最大数テキスト
    [SerializeField] private Text _MaxSummonNumText = default;
    //  召喚カウントの数テキスト
    [SerializeField] private Text _SummonNumText = default;
    //  召喚するコスト（霊力）を管理するクラス
    [SerializeField] private BaseSummonPower _BaseSummonPower = default;
    //  エイムスライダー（左・真ん中・右）
    [SerializeField] private AimSlider[] _AimSliders = default;
    //  DirectionalLight
    [SerializeField] private GameObject _Light = default;
    //  よ〜い画像
    [SerializeField] private GameObject _ImageReady = default;
    //  はじめっ！画像
    [SerializeField] private GameObject _ImageGo = default;
    //  チュートリアルBGM
    [SerializeField] private AudioClip _TutorialBgmClip = default;
    //  通常BGM
    [SerializeField] private AudioClip _NormalBgmClip = default;
    //  ラストステージBGM
    [SerializeField] private AudioClip _LastBattleBgmClip = default;
    //  勝利BGM
    [SerializeField] private AudioClip _VictoryBgmClip = default;
    //  勝利時SE
    [SerializeField] private AudioClip _VictorySeClip = default;
    //  敗北BGM
    [SerializeField] private AudioClip _LoseBgmClip = default;
    //  ロードするステージのプレハブ
    [SerializeField] private GameObject[] _StagePrefab = default;
    //  ゲーム開始時SE
    [SerializeField] private AudioClip _GameStartSeClip = default;
    //  レーンスライダー
    [SerializeField] private AimSlider[] _AimSlider = default;
    //  プレイヤー召喚SE
    [SerializeField] private AudioClip _PlayerSummonClip = default;
    //  敵召喚SE
    [SerializeField] private AudioClip _EnemySummonClip = default;
    //  プレイヤー召喚エフェクトプレハブ
    [SerializeField] private GameObject _PlayerEffSummonPrefab = default;
    //  敵召喚エフェクトプレハブ
    [SerializeField] private GameObject _EnemyEffSummonPrefab = default;
    //  勝利画像
    [SerializeField] private GameObject _VictoryImage = default;
    //  敗北画像
    [SerializeField] private GameObject _LoseImage = default;
    //  ゲームクリアのキャンバスグループ
    [SerializeField] private GameObject _GameClearCanvas = default;
    //  フェードIO
    [SerializeField] private FadeIO _FadeIO = default;
    //  全すてぇじくりあー
    [SerializeField] private GameObject _GameClearMessage01 = default;
    //  おめでとう！
    [SerializeField] private GameObject _GameClearMessage02 = default;
    //  ゲームクリアSE1
    [SerializeField] private AudioClip _GameClearSe01Clip = default;
    //  ゲームクリアSE2
    [SerializeField] private AudioClip _GameClearSe02Clip = default;
    //  ゲームクリアBGM
    [SerializeField] private AudioClip _GameClearBGMClip = default;

    //---------------------チュートリアル用-----------------------
    //  要石の耐久力用
    [SerializeField] private GameObject _KeyStoneFrame = default;
    //  敵基地の耐久力用
    [SerializeField] private GameObject _EnemyBaseFrame = default;
    //  霊力用
    [SerializeField] private GameObject _SummonPowerFrame = default;
    //  矢印用
    [SerializeField] private GameObject _LaneArrowFrame = default;
    //  召喚ボタン用
    [SerializeField] private GameObject _SummonButtonFrame = default;
    //  最大召喚数用
    [SerializeField] private GameObject _SummonMaxFrame = default;


    //  ステージのID
    public enum StageID
    {
        STAGE_01,
        STAGE_02,
        STAGE_03,

        STAGE_MAX
    };
    public enum PlayerType
    {
        TSURU,  // 0
        NEZUMI, // 1
        SARU,   // 2
        INU,    // 3
        TORA,   // 4

        MAX,
    };
    public enum EnemyType
    {
        HIYOKO,  // 0
        HITSUJI, // 1
        PENGIN,  // 2
        MOGURA,  // 3
        ONI,     // 4

        MAX,
    };
    public enum CharacterType
    {
        NONE = -1,
        PLAYER,
        ENEMY,

        MAX
    }
    //  進むレーンのタイプ（左・真ん中・右）
    public enum LaneType
    {
        L,      //  左
        M,      //  真ん中
        R,      //  右

        MAX
    }

    //  シングルトンなインスタンス
    public static GameManager _Instance
    {
        get; private set;
    }

    //  最大召喚数
    private int _PlayerSpawnMax = 50;
    private int _EnemySpawnMax = 50;
    //  このステージで使われる敵のプレハブリスト
    private GameObject[] _EnemyPrefabs;
    //  プレイヤーの召喚獣達のGameObjectリスト
    private List<GameObject> _Players;
    //  敵の召喚獣達のGameObjectリスト
    private List<GameObject> _Enemies;
    //  ゲーム開始前と終了後の待機時間
    private WaitForSeconds _StartWait;
    private WaitForSeconds _EndWait;
    private WaitForSeconds _EndWait2;
    //  ゲームの勝者
    private CharacterType _GameWinner;
    //  メッセージパネルのインスタンス
    private List<GameObject> _MsgPanelList;
    //  大メッセージパネルのインスタンス
    private List<GameObject> _TutorialMsgPanelList;
    //  前回のメッセージが終了しているかどうかのフラグ
    private bool _SentPrevMsg;
    //  前回のプレイヤー数
    private int _prevPlayerNum;
    //  データベースへのリクエストが成功したかどうか
    private int _HasReceivedResult;
    //  敵をポップさせる間隔
    private float _EnemyPopInterval;
    //  AudioSource
    private AudioSource[] _AudioSource;
    //  _AudioSourceのタイプ
    public enum AudioType
    {
        BGM,
        SFX,
        SFX_ENEMY,
        SFX_DAMAGE,
        SFX_KEYSTONE_CHARGE,
        SFX_MOUSE,
        SFX_SUMMON,

        MAX
    }

    //  EnemyBaseのパラメータリスト（敵の出現率とか）
    private StageData _EnemyBaseDataList;
    //  プレイヤー召喚エフェクト
    private ParticleSystem _PlayerSummonEffect;
    //  敵召喚エフェクト
    private ParticleSystem _EnemySummonEffect;



    private void Awake()
    {
        //*******************************************************
        //  ONLY DEBUG
        //********************************************************
#if UNITY_EDITOR
        //  デバッグ用にオブジェクトを生成

        //  FadeManagerがない時は生成する
        if (!GameObject.Find("FadeManager")) Instantiate(_FadeManager);
        //  StageIdManagerがない時は生成する
        if (!GameObject.Find("StageIdManager")) Instantiate(_StageIdMnager);
#endif
        //  _Basesに陣営を設定
        _Bases[(int)CharacterType.PLAYER].GetComponent<BaseHealth>().SetCharaType(CharacterType.PLAYER);
        _Bases[(int)CharacterType.ENEMY].GetComponent<BaseHealth>().SetCharaType(CharacterType.ENEMY);

        //  待機時間設定
        _StartWait = new WaitForSeconds(_StartDelay);
        _EndWait = new WaitForSeconds(_EndDelay);
        _EndWait2 = new WaitForSeconds(_EndDelay);

        //  リスト初期化
        _Players = new List<GameObject>();
        _Enemies = new List<GameObject>();
        _MsgPanelList = new List<GameObject>();
        _TutorialMsgPanelList = new List<GameObject>();

        //  召喚数UI設定
        _prevPlayerNum = 0;
        _MaxSummonNumText.text = "/" + _PlayerSpawnMax.ToString();
        _SummonNumText.text = "0";

        //  フラグを初期値にセット
        _HasReceivedResult = -1;
        _SentPrevMsg = true;

        //  シングルトン処理
        if (_Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _Instance = this;

        //  開始前はレーンスライダーを無効化
        for (int i = 0; i < (int)LaneType.MAX; i++)
            _AimSlider[i].SetButtonActive(false);

        //--------------------------------------------------
        //  ステージ設定
        //--------------------------------------------------
        //  敵プレハブのパス
        string[] _EnemyPrefabPath = new string[(int)EnemyType.MAX]
        {
            "Prefabs/Enemy/LOVEDUCK",       // HIYOKO, 
            "Prefabs/Enemy/sheep",          // HITSUJI,
            "Prefabs/Enemy/penguin",        // PENGIN, 
            "Prefabs/Enemy/mole",           // MOGURA, 
            "Prefabs/Enemy/Boximon Fiery",  // ONI,    
        };
        //  ステージの敵プレハブ
        GameObject[] _StageEnemyPrefabs =
        {
            (GameObject)Resources.Load(_EnemyPrefabPath[(int)EnemyType.HIYOKO]),
            (GameObject)Resources.Load(_EnemyPrefabPath[(int)EnemyType.HITSUJI]),
            (GameObject)Resources.Load(_EnemyPrefabPath[(int)EnemyType.PENGIN]),
            (GameObject)Resources.Load(_EnemyPrefabPath[(int)EnemyType.MOGURA]),
            (GameObject)Resources.Load(_EnemyPrefabPath[(int)EnemyType.ONI]),
        };
        //  各ステージのステージデータ
        StageData[] _StageData =   //(敵リスト配列,敵ベースの体力,敵ポップの間隔（秒）)
        {
            new StageData((int)StageID.STAGE_01),
            new StageData((int)StageID.STAGE_02),
            new StageData((int)StageID.STAGE_03),
        };
        //  このステージのIDを取得
        int stageID = StageIdManager.Instance.GetStageID();

        //*******************************************************
        //  ONLY DEBUG
        //********************************************************
#if UNITY_EDITOR
        //  デバッグ用
        //if (stageID == -1)
        //{
        //    stageID = (int)StageID.STAGE_01;
        //    StageIdManager.Instance.SetStageID(stageID);
        //}
        //  初回プレイかどうか
        //PlayerPrefsBool.SetBool("isPlayingFirst", false);
        //  すぐに勝利にする
        //_Bases[(int)CharacterType.ENEMY].GetComponent<BaseHealth>().SetIsDead(true);
        //  クリア数を設定
        //StageSelectManager.SetStageClearNum( 3 );
#endif

        Assert.IsFalse
       (
           (stageID < 0) || (stageID >= (int)StageID.STAGE_MAX),
           "stageIDが有範囲外です"
       );
        _EnemyBaseDataList = new StageData(stageID);

        //  該当するステージプレハブを生成
        Instantiate(_StagePrefab[stageID]);

        //  ステージの情報をセット
        _EnemyPrefabs = _StageEnemyPrefabs;
        _Bases[(int)CharacterType.ENEMY].GetComponent<BaseHealth>()
            .SetMaxHP(_StageData[stageID]._BaseHP);
        _EnemyPopInterval = _StageData[stageID]._PopInterval;
        //  最終ステージだけライトを変更
        if (stageID == (int)StageID.STAGE_MAX-1)
        {
            //  暗くて紫色な感じに設定
            _Light.transform.rotation = Quaternion.Euler(new Vector3(-80,-30,0));
            _Light.GetComponent<Light>().color = new Color(50,0,50);
        }






    }

    void Start()
    {
        //  開始前に勝者はいない
        _GameWinner = CharacterType.NONE;

        _AudioSource = GetComponents<AudioSource>(); //  コンポーネントを取得

        //  メインループ開始
        StartCoroutine(GameLoop());
    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(Tutorial());
        yield return StartCoroutine(GameStarting());
        yield return StartCoroutine(GamePlaying());
        yield return StartCoroutine(GameEnding());
        yield return StartCoroutine(DataSaving());

    }

    //-----------------------------------------------------------------
    //  チュートリアル
    //-----------------------------------------------------------------
    private IEnumerator Tutorial()
    {
        if (PlayerPrefsBool.GetBool("isPlayingFirst", true))
        {
            //  ルール説明
            yield return StartCoroutine(ExplainAboutRules());

            //  召喚について
            yield return StartCoroutine(ExplainAboutSummon());
        }

        yield return null;
    }

    //-----------------------------------------------------------------
    //  ゲーム開始前
    //-----------------------------------------------------------------
    private IEnumerator GameStarting()
    {
        Debug.Log("GameStarting");

        //  このステージのIDを取得
        int stageID = StageIdManager.Instance.GetStageID();

        //  ステージBGMを設定
        if (stageID == (int)StageID.STAGE_MAX - 1)    //  最後のIDのステージはBGMを変える
        {
            _AudioSource[(int)AudioType.BGM].clip = _LastBattleBgmClip;
        }
        else _AudioSource[(int)AudioType.BGM].clip = _NormalBgmClip;
        _AudioSource[(int)AudioType.BGM].Play();    //  再生

        ResetAllCharacters();
        DisableCharacterControl();

        //  ゲーム開始前メッセージ
        float msg1speed = 1.0f; //  メッセージ１のポップにかかる時間
        float msg1wait  = 3.0f; //  メッセージ１の表示時間
        float msg2speed = 0.2f; //  メッセージ２のポップにかかる時間
        float msg2wait  = 1.0f; //  メッセージ２の表示時間
        yield return StartCoroutine(PreGameMessage
                                    (
                                        msg1speed, msg1wait,    //よ〜い
                                        msg2speed, msg2wait     //はじめっ！
                                    ));

        //  敵を一定間隔でポップさせる
        StartCoroutine(PopEnemyInIntervals(_EnemyPopInterval));

        //  霊力チャージ開始
        _Bases[(int)CharacterType.PLAYER].GetComponent<BaseSummonPower>().StartStackPower();
    }

    //-----------------------------------------------------------------
    //  ゲーム中
    //-----------------------------------------------------------------
    private IEnumerator GamePlaying()
    {
        Debug.Log("GamePlaying");

        //  開始前はレーンスライダーを有効化
        for (int i = 0; i < (int)LaneType.MAX; i++)
            _AimSlider[i].SetButtonActive(true);

        EnableCharacterControl();

        //  どちらかのベースのHPが0になるまで
        while (!OneBaseLeft())
        {
            //  死んでいるキャラはリストから削除
            DeleteDeadObjectFromList(_Players);
            DeleteDeadObjectFromList(_Enemies);

            //  召喚数UIを更新
            UpdateSummonNumUI();

            yield return null;
        }

        //  ゲーム中のUIを無効化する
        _InGameUI.SetActive(false);


        //  全キャラDead
        DeadAllCharacters();
        DeleteDeadObjectFromList(_Players);
        DeleteDeadObjectFromList(_Enemies);


        yield return null;
    }

    //-----------------------------------------------------------------
    //  ゲーム結果発表
    //-----------------------------------------------------------------
    private IEnumerator GameEnding()
    {
        Debug.Log("GameEnding");

        //  コンポーネントを停止
        DisableCharacterControl();

        //  邪魔なのでスポーンポイントを消す
        foreach (GameObject obj in _PlayerSpawnObject) Destroy(obj);
        foreach (GameObject obj in _EnemySpawnObject) Destroy(obj);

        //  ゲームの勝者を決定
        _GameWinner = GetGameWinner();

        Debug.Log("ゲームの勝者は " +_GameWinner.ToString() + "です！");

        BaseHealth loseSide = null;

        //  演出カメラに切り替え
        if (_GameWinner == CharacterType.PLAYER)        //  勝利カメラに切り替え
        {
            //  負けた敵ベースのコンポーネントを取得
            loseSide = _Bases[(int)CharacterType.ENEMY].GetComponent<BaseHealth>();
            //  カメラ切り替え
            _MainCamera.SetActive(false);
            _WinCamera.SetActive(true);
            _LoseCamera.SetActive(false);
        }
        else if (_GameWinner == CharacterType.ENEMY)    //  敗北カメラに切り替え
        {
            //  負けたプレイヤーベースのコンポーネントを取得
            loseSide = _Bases[(int)CharacterType.PLAYER].GetComponent<BaseHealth>();
            //  カメラ切り替え
            _MainCamera.SetActive(false);
            _WinCamera.SetActive(false);
            _LoseCamera.SetActive(true);
        }

        //  BGMストップ
        _AudioSource[(int)AudioType.BGM].Stop();

        //  霊力が溜まらないようにする
        _Bases[(int)CharacterType.PLAYER].GetComponent<BaseSummonPower>().StopStackPower();

        //  ベースの死亡演出
        loseSide.DeadStaging();    //  死亡演出再生

        //  演出終了まで待つ
        while (!loseSide.GetIsDeadStagingEnd())
        {
            yield return null;
        }

        //  勝ったキャラクタータイプによってBGMを変える
        ChangeBGMByResult(_GameWinner);

        //  結果演出SEを鳴らす
        PlayResultSE(_GameWinner);

        //  演出後は結果メッセージを設定
        string message = EndMessage();

        //  結果表示テキストの設定
        ResultTextSettings(_GameWinner);

        //  テキストにメッセージを設定して有効化
        _ResultText.text = message;
        _ResultText.gameObject.SetActive(true);

        yield return _EndWait;

        //  勝ったキャラクタータイプによって立ち絵を変える
        ChangeImageByResult(_GameWinner);

        yield return _EndWait2;

        //  敗北ならセーブせずにそのままステージセレクトへ戻る
        if (_GameWinner == CharacterType.ENEMY)
        {
            //  ステージセレクトシーンへ移行
            ChangeScene.Execute("SceneStageSelect");
        }
    }

    //-----------------------------------------------------------------
    //  勝った場合はデータ記録フェイズ
    //-----------------------------------------------------------------
    private IEnumerator DataSaving()
    {
        Debug.Log("DataSaving");

        //  勝った場合はステージクリア数をデータ記録
        if (_GameWinner == CharacterType.PLAYER)
        {
            if (StageIdManager.Instance.GetStageID() == (int)StageID.STAGE_01)
            {
                //初回フラグOFFしてSAVE
                PlayerPrefsBool.SetBool("isPlayingFirst", false);
                PlayerPrefs.Save();

                // デバッグ表示
                Debug.Log("初回フラグOFF");
            }

            //  クリア数を増やして
            int clearNum = StageSelectManager.GetStageClearNum();
            clearNum++;
            //  satic変数にセット
            StageSelectManager.SetStageClearNum(clearNum);

            //  ローカルに保存
            PlayerPrefs.SetInt(TitleManager._keyClearNum, StageSelectManager.GetStageClearNum());
            PlayerPrefs.Save();

            //  ローカルセーブ完了メッセージ
            yield return StartCoroutine(Message("ローカルセーブを完了しました！", 1f));

            //  データベースへ保存リクエスト
            StartCoroutine(CreateJsonToWww());
            //  結果が帰ってくるまで表示
            yield return　StartCoroutine(MessageUntilResult("データを保存しています"));

            if (_HasReceivedResult == 0)    //  送信失敗
            {
                yield return StartCoroutine(Message("データベースアクセスに失敗しました", 1f));
            }
            else if(_HasReceivedResult == 1)   //  送信成功
            {
                //  保存完了！
                yield return StartCoroutine(Message("データベースにセーブが完了しました！", 1f));
            }

            //  全ステージクリア演出
            yield return StartCoroutine(AllStageClear(clearNum));

            //  5秒待機
            yield return new WaitForSeconds(5f);

            yield return StartCoroutine(Message("ステージセレクトに戻ります", 1f));

            //  ステージセレクトシーンへ移行
            ChangeScene.Execute("SceneStageSelect");
        }


    }

    //-----------------------------------------------------------------
    //  結果表示
    //-----------------------------------------------------------------
    private string EndMessage()
    {
        string message = "ひきわけ！";

        if (_GameWinner == CharacterType.NONE) return message;
        else
        {
            if (_GameWinner == CharacterType.PLAYER)
            {
                message = "勝利";
            }
            else if (_GameWinner == CharacterType.ENEMY)
            {
                message = "敗北...";
            }
        }

        return message;
    }

    //-----------------------------------------------------------------
    //  全てのキャラを死亡状態にする
    //-----------------------------------------------------------------
    private void DeadAllCharacters()
    {
        //  プレイヤーをリセット
        for (int i = 0; i < _Players.Count; i++)
        {
            _Players[i].GetComponent<PlayerManager>().DstroyObject();

        }
        //  敵をリセット
        for (int i = 0; i < _Enemies.Count; i++)
        {
            _Enemies[i].GetComponent<EnemyManager>().DstroyObject();
        }
    }

    //-----------------------------------------------------------------
    //  全てのキャラをリセットする
    //-----------------------------------------------------------------
    private void ResetAllCharacters()
    {
        //  プレイヤーをリセット
        for (int i = 0; i < _Players.Count; i++)
        {
            _Players[i].GetComponent<PlayerManager>().Reset();

        }
        //  敵をリセット
        for (int i = 0; i < _Enemies.Count; i++)
        {
            _Enemies[i].GetComponent<EnemyManager>().Reset();
        }
    }

    //-----------------------------------------------------------------
    //  キャラクターの動作を有効にする
    //-----------------------------------------------------------------
    private void EnableCharacterControl()
    {
        //  プレイヤー有効化
        for (int i = 0; i < _Players.Count; i++)
        {
            _Players[i].GetComponent<PlayerManager>().EnableControl();

        }
        //  敵有効化
        for (int i = 0; i < _Enemies.Count; i++)
        {
            _Enemies[i].GetComponent<EnemyManager>().EnableControl();
        }
    }

    //-----------------------------------------------------------------
    //  キャラクターの動作を無効にする
    //-----------------------------------------------------------------
    private void DisableCharacterControl()
    {
        //  プレイヤー無効化
        for (int i = 0; i < _Players.Count; i++)
        {
            _Players[i].GetComponent<PlayerManager>().DisableControl();

        }
        //  敵無効化
        for (int i = 0; i < _Enemies.Count; i++)
        {
            _Enemies[i].GetComponent<EnemyManager>().DisableControl();
        }
    }

    //-----------------------------------------------------------------
    //  召喚ボタン押し下時の処理
    //-----------------------------------------------------------------
    public void OnClickSummonButton(int type)
    {
        Assert.IsFalse
        (
            (type < 0) || (type >= (int)PlayerType.MAX),
            "OnClickSummonButtonの引数[type]が有効ではありません"
        );

        //  ボタンで選択したレーンに生成する
        int laneId = -1;
        for (int i = (int)LaneType.L; i < (int)LaneType.MAX; i++)
        {
            if (_AimSliders[i].IsSelected())
            {
                laneId = i;
            }
        }

        //  レーン選択せずに召喚ボタンを押した場合
        if (laneId == -1)
        {
            Debug.Log("召喚するレーンが選択されていません！");
            StartCoroutine(Message("召喚するレーンが選択されていません！", 1.0f));
            return;
        }

        //  対面のレーンのIDを設定
        int otherId = -1;
        if (laneId == (int)LaneType.L) otherId = (int)LaneType.R;
        if (laneId == (int)LaneType.M) otherId = (int)LaneType.M;
        if (laneId == (int)LaneType.R) otherId = (int)LaneType.L;

        //  オブジェクト生成&コンポーネントを追加
        if (_Players.Count < _PlayerSpawnMax)
        {
            //  霊力がコスト以上なら
            if (_BaseSummonPower.GetPowerValue() >= _BaseSummonPower._playerDataList[type]._Cost)
            {
                //  霊力からコストを引く
                _BaseSummonPower.OnClickSummonButton(type);

                _Players.Add(Instantiate
                                        (
                                            _PlayerPrefabs[type],
                                            _PlayerSpawnObject[laneId].transform.position,
                                            Quaternion.Euler(new Vector3(0, 0, 0))
                                        )
                         );
                _Players.Last().GetComponent<PlayerManager>().Setup(type);
                _Players.Last().GetComponent<PlayerManager>().
                _Attack.SetAttackerTagName("Player");

                //　目的地は対面の敵スポーン
                _Players.Last().GetComponent<PlayerManager>()
                    .SetMoveTarget(_EnemySpawnObject[otherId].transform.position);

                //  召喚エフェクトの生成
                _PlayerSummonEffect = Instantiate(_PlayerEffSummonPrefab).GetComponent<ParticleSystem>();
                _PlayerSummonEffect.transform.position = _PlayerSpawnObject[laneId].transform.position;

                //  プレイヤー召喚SE再生
                _AudioSource[(int)AudioType.SFX_SUMMON].clip = _PlayerSummonClip;
                _AudioSource[(int)AudioType.SFX_SUMMON].Play();
            }
            else
            {
                Debug.Log("霊力が不足しています！");
                StartCoroutine(Message("霊力が不足しています！", 1.0f));
            }
        }
        else
        {
            Debug.Log("プレイヤーは最大数召喚しています！");
            StartCoroutine(Message("プレイヤーは最大数召喚しています！", 1.0f));
        }
    }

    //-----------------------------------------------------------------
    //  敵を出現させる
    //-----------------------------------------------------------------
    public void PopEnemy()
    {
        //---------------------------------------------------------------------------
        //  湧かす敵のタイプの抽選
        //---------------------------------------------------------------------------
        //  出現率0のものは省いてリストに追加
        List<int> popIdList = new List<int>();
        for (int i = 0; i < (int)EnemyType.MAX; i++)
        {
            if (_EnemyBaseDataList._SpawnRate[i] <= 0.0f) continue;
            popIdList.Add( i );
        }
        //  リストからランダムにタイプを決定
        int type = popIdList.PopRandomElement();

        //---------------------------------------------------------------------------
        //  敵の出現率をもとに出現するかどうかを決定
        //---------------------------------------------------------------------------
        float rate = UnityEngine.Random.Range(0f, 100f);

        //  基準値より大きいならポップしない
        if (rate > _EnemyBaseDataList._SpawnRate[type]) return;

        

        Assert.IsFalse
        (
            (type < 0) || (type >= _EnemyPrefabs.Length),
            "OnClickSummonButtonの引数[type]が有効ではありません"
        );

        //  湧かすレーンのIDを抽選
        int laneId = UnityEngine.Random.Range((int)LaneType.L, (int)LaneType.MAX);

        //  対面のレーンのIDを設定
        int otherId = -1;
        if (laneId == (int)LaneType.L) otherId = (int)LaneType.R;
        if (laneId == (int)LaneType.M) otherId = (int)LaneType.M;
        if (laneId == (int)LaneType.R) otherId = (int)LaneType.L;


        //  オブジェクト生成&コンポーネントを追加
        if (_Enemies.Count <= _EnemySpawnMax)
        {
            _Enemies.Add(Instantiate
                                    (
                                        _EnemyPrefabs[type],
                                        _EnemySpawnObject[laneId].transform.position,
                                        Quaternion.Euler(new Vector3(0, 0, 0))
                                    )
                     );
            _Enemies.Last().GetComponent<EnemyManager>().Setup(type);
            _Enemies.Last().GetComponent<EnemyManager>().
                _Attack.SetAttackerTagName("Enemy");

            //　目的地は対面のプレイヤースポーン
            _Enemies.Last().GetComponent<EnemyManager>()
                .SetMoveTarget(_PlayerSpawnObject[otherId].transform.position);

            //  召喚エフェクトの生成
            _EnemySummonEffect = Instantiate(_EnemyEffSummonPrefab).GetComponent<ParticleSystem>();
            _EnemySummonEffect.transform.position = _EnemySpawnObject[laneId].transform.position;

            //  敵召喚SE再生
            _AudioSource[(int)AudioType.SFX_ENEMY].clip = _EnemySummonClip;
            _AudioSource[(int)AudioType.SFX_ENEMY].Play();

        }
        else
        {
            Debug.Log("敵は最大数召喚しています！");
        }
    }

    //-----------------------------------------------------------------
    //  一定間隔で敵を出現させる(intervals:秒)
    //-----------------------------------------------------------------
    private IEnumerator PopEnemyInIntervals(float intervals)
    {
        //  ステージ終了まで敵をポップさせる
        while(!OneBaseLeft())
        {
            PopEnemy();     
            yield return new WaitForSeconds(intervals);
        }
        
    }

    //-----------------------------------------------------------------
    //  死んでいるキャラをリストから削除
    //-----------------------------------------------------------------
    private void DeleteDeadObjectFromList(List<GameObject>list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null) continue;   //  nullなら無視

            //  生存中に死亡フラグがONになったらリストから削除する
            if (list[i].GetComponent<CharacterHealth>().IsDead())
            {
                Destroy(list[i].gameObject);       //  オブジェクトも削除する
                list.Remove(list[i]);              //  リストからも削除する
            }
        }
    }

    //-----------------------------------------------------------------
    //  ゲーム終了判定
    //-----------------------------------------------------------------
    private bool OneBaseLeft()
    {
        int numBaseLeft = 0;

        //  どちらかのベースが生き残った状態
        for (int i = 0; i < _Bases.Length; i++)
        {
            if (!_Bases[i].GetComponent<BaseHealth>().GetIsDead())
                numBaseLeft++;
        }

        return numBaseLeft <= 1;
    }

    //-----------------------------------------------------------------
    //  ゲームの勝者を返す
    //-----------------------------------------------------------------
    private CharacterType GetGameWinner()
    {
        //  生き残ったベースに応じた勝者を設定する

        for (int i = 0; i < _Bases.Length; i++)
        {
            if (!_Bases[i].GetComponent<BaseHealth>().GetIsDead())
                return _Bases[i].GetComponent<BaseHealth>().GetCharaType();
        }

        return CharacterType.NONE;
    }

    //-----------------------------------------------------------------
    //  ゲーム終了時のテキストの設定
    //-----------------------------------------------------------------
    private void ResultTextSettings(CharacterType winner)
    {
        if (winner == CharacterType.PLAYER)        //  勝者がプレイヤー
        {
            //  結果表示テキストの色を指定
            _ResultText.GetComponent<Text>().color = Color.white;
            _ResultText.GetComponent<Text>().fontSize = 180;
            _ResultText.GetComponent<Shadow>().effectColor = Color.black;

        }
        else if (winner == CharacterType.ENEMY)    //  勝者が敵
        {
            //  結果表示テキストのパラメータを指定
            _ResultText.GetComponent<Text>().color = Color.black;
            _ResultText.GetComponent<Text>().fontSize = 130;
            _ResultText.GetComponent<Shadow>().effectColor = Color.white;
        }
    }

    //-----------------------------------------------------------------
    //  メッセージパネルの設定(内容,表示する秒数)
    //-----------------------------------------------------------------
    private IEnumerator Message(string message, float seconds)
    {
        if (_SentPrevMsg)
        {
            _SentPrevMsg = false;

            string msg = message;
            float sec = seconds;

            //  キャンバスの子オブジェクトとして生成
            _MsgPanelList.Add(Instantiate(_MessagePanelPrefab));
            _MsgPanelList.Last().transform.SetParent(_Canvas.transform, false);

            //  メッセージを設定してポップアップ
            _MsgPanelList.Last().GetComponentInChildren<Text>().text = message;
            _MsgPanelList.Last().GetComponent<Popup>().OnOpen();

            //  sec秒待つ
            yield return new WaitForSeconds(sec);

            //  スケールダウンしてオブジェクトを削除
            _MsgPanelList.Last().GetComponent<Popup>().OnClose();

            _SentPrevMsg = true;
        }
    }

    //-----------------------------------------------------------------
    //  メッセージパネルの設定(内容)
    //-----------------------------------------------------------------
    private IEnumerator ClickMessage(string message)
    {
        string msg = message;

        //  キャンバスの子オブジェクトとして生成
        _MsgPanelList.Add(Instantiate(_TutorialMessagePanelPrefab));
        _MsgPanelList.Last().transform.SetParent(_TutorialObject.transform, false);

        //  メッセージを設定してポップアップ
        _MsgPanelList.Last().GetComponentInChildren<Text>().text = message;
        _MsgPanelList.Last().GetComponent<Popup>().OnOpen();

        //  クリックされるまで待つ
        yield return new WaitUntil( () => Input.GetMouseButtonDown(0) );

        //  スケールダウンしてオブジェクトを削除
        _MsgPanelList.Last().GetComponent<Popup>().OnClose();

        //  0.5秒のウェイトをかける
        yield return new WaitForSeconds(0.5f);
    }

    //-----------------------------------------------------------------
    //  メッセージパネルの設定(リクエスト結果が帰ってきたら消す)
    //-----------------------------------------------------------------
    private IEnumerator MessageUntilResult(string message)
    {
        string msg = message;

        //  キャンバスの子オブジェクトとして生成
        _MsgPanelList.Add(Instantiate(_MessagePanelPrefab));
        _MsgPanelList.Last().transform.SetParent(_Canvas.transform, false);

        //  メッセージを設定してポップアップ
        _MsgPanelList.Last().GetComponentInChildren<Text>().text = message;
        _MsgPanelList.Last().GetComponent<Popup>().OnOpen();

        //  条件がTrueになるまでまつ
        yield return new WaitUntil( () => _HasReceivedResult != -1);

        //  スケールダウンしてオブジェクトを削除
        _MsgPanelList.Last().GetComponent<Popup>().OnClose();
    }

    //-----------------------------------------------------------------
    //  召喚数UIを更新する
    //-----------------------------------------------------------------
    private void UpdateSummonNumUI()
    {
        //  プレイヤーの数が前回と違っていたら
        if (_prevPlayerNum != _Players.Count)
        {
            //  召喚数を更新
            _SummonNumText.text = _Players.Count.ToString();
        }
        //  前回のプレイヤー数を記録
        _prevPlayerNum = _Players.Count;
    }

    //-----------------------------------------------------------------
    //  レコードを作成してデータベースに送信する
    //-----------------------------------------------------------------
    private IEnumerator CreateJsonToWww()
    {
        // APIが設置してあるURLパス
        string sTgtURL = "http://localhost/NameMatchingSystem/onmyou/createMessage";

        int clearNum = StageSelectManager.GetStageClearNum();

        // Wwwを利用して json データ設定をリクエストする
        yield return StartCoroutine(SetMessage(sTgtURL, clearNum, CallbackApiSuccess, CallbackWwwFailed));
    }

    //-----------------------------------------------------------------
    //  WWWを利用してデータベースにリクエストする
    //-----------------------------------------------------------------
    private IEnumerator SetMessage(string url, int clearNum, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("ClearNum", clearNum);

        // WWWを利用してリクエストを送る
        var webRequest = UnityWebRequest.Post(url, form);

        //タイムアウトの指定
        webRequest.timeout = 10;

        // WWWレスポンス待ち
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            if (null != cbkSuccess)
            {
                cbkSuccess("データベースに登録しました！");
            }
        }
    }

    //-----------------------------------------------------------------
    //  リクエストが成功した時のコールバック
    //-----------------------------------------------------------------
    private void CallbackApiSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        Debug.Log("リクエスト成功\n" + response);

        //  結果が成功なら１
        _HasReceivedResult = 1;
    }

    //-----------------------------------------------------------------
    //  リクエストが失敗した時のコールバック
    //-----------------------------------------------------------------
    private void CallbackWwwFailed()
    {
        // データ送信に失敗した
        Debug.Log("データベースアクセスに失敗しました");

        //  結果が成功なら0
        _HasReceivedResult = 0;
    }
    //-----------------------------------------------------------------
    //  ゲーム開始前のメッセージ
    //-----------------------------------------------------------------
    private IEnumerator PreGameMessage(float speed, float interval, float speed2, float interval2)
    {
        //  ロード後いきなり始まらないように1秒ウェイト
        yield return new WaitForSeconds(1f);

        //  ゲーム開始SE設定
        _AudioSource[(int)AudioType.SFX].clip = _GameStartSeClip;
        _AudioSource[(int)AudioType.SFX].Play();

        _ImageReady.GetComponent<Popup>().SetCustomFlag(true);
        _ImageReady.SetActive(true);
        _ImageReady.GetComponent<Popup>().OnOpen(speed);

        yield return new WaitForSeconds(interval);

        _ImageReady.GetComponent<Popup>().OnClose(speed);

        _ImageGo.GetComponent<Popup>().SetCustomFlag(true);
        _ImageGo.SetActive(true);
        _ImageGo.GetComponent<Popup>().OnOpen(speed2);

        yield return new WaitForSeconds(interval2);

        _ImageGo.GetComponent<Popup>().OnClose(speed2);
    }

    //-----------------------------------------------------------------
    //  結果によって立ち絵を変える
    //-----------------------------------------------------------------
    private void ChangeImageByResult(CharacterType winner)
    {
        if (winner == CharacterType.PLAYER)
        {
            _VictoryImage.SetActive(true);                           //  立ち絵表示
        }
        else if (winner == CharacterType.ENEMY)
        {
            _LoseImage.SetActive(true);                               //  立ち絵表示
        }
    }

    //-----------------------------------------------------------------
    //  結果によってBGMを変える
    //-----------------------------------------------------------------
    private void ChangeBGMByResult(CharacterType winner)
    {     
        if (winner == CharacterType.PLAYER)
        {
            _AudioSource[(int)AudioType.BGM].clip = _VictoryBgmClip; // BGMを設定
            _AudioSource[(int)AudioType.BGM].Play();    //  再生
        }
        else if (winner == CharacterType.ENEMY)
        {
            _AudioSource[(int)AudioType.SFX].clip = _LoseBgmClip;     // BGMを設定
            _AudioSource[(int)AudioType.SFX].Play();    //  再生
        }

        
    }

    //-----------------------------------------------------------------
    //  結果SEを再生
    //-----------------------------------------------------------------
    private void PlayResultSE(CharacterType winner)
    {
        if (_GameWinner == CharacterType.PLAYER)
        {
            _AudioSource[(int)AudioType.SFX].clip = _VictorySeClip;
            _AudioSource[(int)AudioType.SFX].Play();
        }
        else if (winner == CharacterType.ENEMY)
        {

        }
        
    }

    //-----------------------------------------------------------------
    //  枠オブジェクトを点滅させて削除する
    //-----------------------------------------------------------------
    private IEnumerator BlinkObject(GameObject frame)
    {
        GameObject obj = frame;

        //  点滅開始
        yield return  StartCoroutine(obj.GetComponent<Blink>().Blinking());
    }

    //-----------------------------------------------------------------
    //  ルール説明
    //-----------------------------------------------------------------
    private IEnumerator ExplainAboutRules()
    {
        //  チュートリアルBGM再生
        _AudioSource[(int)AudioType.BGM].clip = _TutorialBgmClip;
        _AudioSource[(int)AudioType.BGM].Play();

        yield return StartCoroutine(ClickMessage("-------------ゲーム説明-------------"));
        yield return StartCoroutine(ClickMessage("このゲームは都を守る結界の柱となる「要石」を\nもののけ達から守るゲームです"));
        yield return StartCoroutine(ClickMessage("自称天才陰陽師の折紙ちゃんと協力して\n敵の基地を壊して追い返しましょう！"));

        yield return StartCoroutine(ClickMessage("-------------ルール説明-------------"));
        yield return StartCoroutine(ClickMessage("プレイヤーと敵それぞれに本拠地を持っており、\nそれぞれ耐久力があります"));
        
        yield return StartCoroutine(BlinkObject(_KeyStoneFrame));
        yield return StartCoroutine(BlinkObject(_EnemyBaseFrame));

        yield return StartCoroutine(ClickMessage("プレイヤーの本拠地は「要石」と呼ばれ、\nこれの耐久力が先に０なると負けになります"));
        yield return StartCoroutine(ClickMessage("逆に敵基地の耐久力を先に０にするとプレイヤーの勝ちとなります"));

        yield return null;
    }

    //-----------------------------------------------------------------
    //  召喚についての説明
    //-----------------------------------------------------------------
    private IEnumerator ExplainAboutSummon()
    {
        yield return StartCoroutine(ClickMessage("-------------召喚について-------------"));
        yield return StartCoroutine(ClickMessage("折紙ちゃんは折紙の式神を召喚し、敵と戦います"));
        yield return StartCoroutine(ClickMessage("召喚するためには「要石」から供給される\n霊力が必要です"));

        yield return StartCoroutine(BlinkObject(_SummonPowerFrame));

        yield return StartCoroutine(ClickMessage("霊力は一定時間ごとに決まった量ずつ溜まっていきます"));
        yield return StartCoroutine(ClickMessage("また、式神によって召喚に必要な霊力が違います\n霊力の残量には気をつけましょう"));

        yield return StartCoroutine(ClickMessage("では、実際の召喚手順について説明します\n"));
        yield return StartCoroutine(ClickMessage("�@レーンを選択する\nレーンとは要石の前にある３つの矢印のことです"));

        yield return StartCoroutine(BlinkObject(_LaneArrowFrame));

        yield return StartCoroutine(ClickMessage("レーンに召喚された式神はそのレーンに従って進みます"));
        yield return StartCoroutine(ClickMessage("�A召喚ボタンをクリックする\n召喚ボタンとは画面左に配置された５つのボタンのことです"));

        yield return StartCoroutine(BlinkObject(_SummonButtonFrame));

        yield return StartCoroutine(ClickMessage("以上２ステップで式神を召喚できます\nしかし霊力の他に「最大召喚可能数」があります。"));

        yield return StartCoroutine(BlinkObject(_SummonMaxFrame));

        yield return StartCoroutine(ClickMessage("どんなに霊力が余っていてもこの数以上は\n召喚できないので注意してください。"));
        yield return StartCoroutine(ClickMessage("以上でチュートリアルは終了です。\n健闘を祈ります！"));

        yield return new WaitForSeconds(2f);
    }

    //-----------------------------------------------------------------
    //  全ステージクリア演出
    //-----------------------------------------------------------------
    private IEnumerator AllStageClear(int clearNum)
    {
        Debug.Log("clearNum :" + clearNum);
        Debug.Log("STAGE_MAX :" + (int)StageID.STAGE_MAX);

        //  全ステージクリアなら
        if (clearNum >= (int)StageID.STAGE_MAX)
        {
            //GameClearのキャンバスグループを有効化する
            _GameClearCanvas.SetActive(true);

            //  フェードイン・アウト
            yield return StartCoroutine(_FadeIO.StartAnimation());

            //  全ステージくりあー
            _GameClearMessage01.GetComponent<Image>().enabled = true;
            _GameClearMessage01.GetComponent<Popup>().OnOpen();

            //  SE再生
            _AudioSource[(int)AudioType.SFX].clip = _GameClearSe01Clip;
            _AudioSource[(int)AudioType.SFX].Play();

            //  1秒待機
            yield return new WaitForSeconds(1f);

            //  おめでとう！
            _GameClearMessage02.GetComponent<Image>().enabled = true;
            _GameClearMessage02.GetComponent<Popup>().OnOpen();

            //  SE再生
            _AudioSource[(int)AudioType.SFX].clip = _GameClearSe02Clip;
            _AudioSource[(int)AudioType.SFX].Play();

            //  1秒待機
            yield return new WaitForSeconds(1f);

            //  BGM再生
            _AudioSource[(int)AudioType.SFX].clip = _GameClearBGMClip;
            _AudioSource[(int)AudioType.SFX].Play();
        }

        yield return null;
    }
}

//-----------------------------------------------------------------
//  リストからランダムに値を取り出す
//-----------------------------------------------------------------
public static class ListExt
{
    public static T PopRandomElement<T>(this List<T> self)
    {
        var item = self[UnityEngine.Random.Range(0, self.Count)];
        self.Remove(item);
        return item;
    }
}