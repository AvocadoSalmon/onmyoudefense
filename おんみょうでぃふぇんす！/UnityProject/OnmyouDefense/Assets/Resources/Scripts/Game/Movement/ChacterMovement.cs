using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//*****************************************************************
//
//  キャラクター移動クラス
//
//*****************************************************************
public class ChacterMovement : MonoBehaviour
{
    public bool _CanMove;             // 移動していいかのフラグ
    protected bool _HasReached;          // 目的地に到達したかのフラグ
    protected bool _HasEncounted;        // 敵か敵ベースに遭遇したか
    protected Vector3 _TargetPosition;   // 移動先の座標
    protected float _Speed;              // キャラクターの移動スピード
    protected Rigidbody _Rigidbody;      // キャラクターのRigidbody



    private void Awake()
    {
        _Speed = 0.0f;
        _CanMove = true;
        _HasReached = false;
        _HasEncounted = false;
        _TargetPosition = Vector3.zero;
    }

    void Start()
    {
        _Rigidbody = this.GetComponent<Rigidbody>();
    }

    //-----------------------------------------------------------------------
    //  直進する
    //-----------------------------------------------------------------------
    public virtual void Movement() 
    {
        
    }

    public virtual　void Movement(PlayerData playerData)
    {

    }

    public virtual void Movement(EnemyData enemyData)
    {

    }

    //-----------------------------------------------------------------------
    //  当たり判定
    //-----------------------------------------------------------------------
    public virtual void OnTriggerEnter(Collider collision)
    {

    }

    public virtual void OnTriggerExit(Collider other)
    {
        
    }

    //-----------------------------------------------------------------------
    //  _CanMoveのプロパティ
    //-----------------------------------------------------------------------
    public void SetCanMove(bool canMove)
    {
        if(_CanMove != canMove)
        {
            _CanMove = canMove;

            //Debug.Log("_CanMoveが " + _CanMove + " にセットされました");
        }
    }

    public bool GetCanMove()
    {
        return _CanMove;
    }

    //-----------------------------------------------------------------------
    //  _HasReachedのプロパティ
    //-----------------------------------------------------------------------
    public void SetHasReached(bool reached)
    {
        if (_HasReached != reached)
        {
            _HasReached = reached;

            //Debug.Log("_HasReachedが " + _HasReached + " にセットされました");
        }
    }

    public bool GetHasReached()
    {
        return _HasReached;
    }

    //-----------------------------------------------------------------------
    //  _HasEncountedのプロパティ
    //-----------------------------------------------------------------------
    public void SetHasEncounted(bool encount)
    {
        if (_HasEncounted != encount)
        {
            _HasEncounted = encount;

            //Debug.Log("_HasEncounted " + _HasEncounted + " にセットされました");
        }
    }

    public bool GetHasEncounted()
    {
        return _HasEncounted;
    }

    //-----------------------------------------------------------------------
    //  _Speedのプロパティ
    //-----------------------------------------------------------------------
    public void SetSpeed(float speed)
    {
        _Speed = speed;
    }

    public float GetSpeed()
    {
        return _Speed;
    }

    //-----------------------------------------------------------------------
    //  移動先の設定
    //-----------------------------------------------------------------------
    public void SetTargetPosition(Vector3 target)
    {
        _TargetPosition = target;
    }

    //-----------------------------------------------------------------------
    //  移動の停止
    //-----------------------------------------------------------------------
    public void StopMoving()
    {
        //Rigidbodyを停止
        _Rigidbody.velocity = Vector3.zero;
    }


}
