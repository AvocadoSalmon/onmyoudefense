using UnityEngine;
using DG.Tweening;

//*****************************************************************
//
//  プレイヤー移動クラス
//
//*****************************************************************
public class PlayerMovement : ChacterMovement
{
    private Tween _Tween;

    //-----------------------------------------------------------------------
    //  直進する
    //-----------------------------------------------------------------------
    public override void Movement(PlayerData playerData)
    {
        if (_CanMove)
        {
            // 敵ベースへのベクトルを求める
            Vector3 vec = _TargetPosition - base._Rigidbody.position;
            Vector3.Normalize(vec);

            // １秒間で_Speed分移動
            Vector3 movement = vec * base._Speed * Time.deltaTime;

            //  指定位置へ移動(現在座標+移動量)
            base._Rigidbody.AddForce(movement);

            //  移動アニメ再生
            MoveAnimation(playerData);
        }
    }

    //-----------------------------------------------------------------------
    //  当たり判定
    //-----------------------------------------------------------------------
    public override void OnTriggerEnter(Collider collision)
    {
        //敵に当たったら
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //Debug.Log(collision.gameObject.tag + "にHIT");

            //アニメ停止
            if (this._Tween != null) Stop();
            //移動禁止
            base.SetCanMove(false);
            //敵に遭遇した！
            base.SetHasEncounted(true);
            //目的地に到達してない
            base.SetHasReached(false);
        }
        else if (collision.gameObject.CompareTag("EnemyBase"))
        {   //  敵本陣に当たったら
            //Debug.Log(collision.gameObject.tag + "にHIT");

            //アニメ停止
            if (this._Tween != null) Stop();
            //移動禁止
            base.SetCanMove(false);
            //敵に遭遇してない
            base.SetHasEncounted(false);
            //目的地に到達
            base.SetHasReached(true);
        }
    }

    public override void OnTriggerExit(Collider other)
    {
        //  衝突してない時
 
        //敵に遭遇した！
        base.SetHasEncounted(false);
    }

    //-----------------------------------------------------------------------
    //  アニメが付いてなかったので移動時のアニメをDOTWEENNでつける
    //-----------------------------------------------------------------------
    private void MoveAnimation(PlayerData playerData)
    {
        if (this._Tween == null)
        {
            Rotation(playerData);
        }
    }

    //-----------------------------------------------------------------------
    //  DOTWEENNで回転(回転する角度(度), 回転にかかる時間(秒))
    //-----------------------------------------------------------------------
    private void Rotation(PlayerData playerData)
    {
        PlayerData p = playerData;

        // まず-Angleまで振りまで振り上げ
        this._Tween = this._Rigidbody.DORotate(new Vector3(-p._Angle, 0f, 0f), p._Time)
            .SetEase(Ease.InOutQuad).OnComplete(() =>
            {
                // -Angleから+Angleまで振り、ヨーヨーループで振り子運動を繰り返させる
                Sequence sequence = DOTween.Sequence();
                sequence.Append(
                    this._Rigidbody.DORotate(
                        new Vector3(p._Angle, 0f, 0f), 2 * p._Time)
                    .SetEase(Ease.Linear)
                ).SetLoops(-1, LoopType.Yoyo);
                this._Tween = sequence;
            });
    }

    //-----------------------------------------------------------------------
    //  DOTWEENNを停止
    //-----------------------------------------------------------------------
    private void Stop()
    {
        // 今の_Tweenはキャンセルし
        this._Tween.Kill();

        //  nullにする
        this._Tween = null;

        //  強制敵に無回転にする
        this._Rigidbody.rotation = Quaternion.identity;
    }
}
