using UnityEngine;
using System;
using UniRx;

/// <summary>
/// キャラクターの状態（ステート）
/// </summary>
namespace EnemyState
{
    /// <summary>
    /// ステートの実行を管理するクラス
    /// </summary>
    public class StateProcessor
    {
        //ステート本体
        public ReactiveProperty<CharacterState> State { get; set; } = new ReactiveProperty<CharacterState>();

        //実行ブリッジ
        public void Execute() => State.Value.Execute();
    }

    /// <summary>
    /// ステートのクラス
    /// </summary>
    public abstract class CharacterState
    {
        //デリゲート
        public Action ExecAction { get; set; }

        //Update
        public abstract void Update(EnemyManager em);

        //実行処理
        public virtual void Execute()
        {
            if (ExecAction != null) ExecAction();
        }

        //ステート名を取得するメソッド
        public abstract string GetStateName();
    }

    //=================================================================================
    //以下状態クラス
    //=================================================================================

    /// <summary>
    /// 何もしていない状態
    /// </summary>
    public class CharacterStateIdle : CharacterState
    {
        public override string GetStateName()
        {
            return "State:Idle";
        }
        public override void Update(EnemyManager em)
        {
            em.IdleUpdate();
        }
        public override void Execute()
        {
            if (ExecAction != null) ExecAction();
        }
    }

    /// <summary>
    /// 移動している状態
    /// </summary>
    public class CharacterStateMove : CharacterState
    {
        public override string GetStateName()
        {
            return "State:Move";
        }
        public override void Update(EnemyManager em)
        {
            em.MoveUpdate();
        }
        public override void Execute()
        {
            if (ExecAction != null) ExecAction();
        }
    }

    /// <summary>
    /// 攻撃している状態
    /// </summary>
    public class CharacterStateAttack : CharacterState
    {
        public override string GetStateName()
        {
            return "State:Attack";
        }
        public override void Update(EnemyManager em)
        {
            em.AttackUpdate();
        }
        public override void Execute()
        {
            if (ExecAction != null) ExecAction();
        }
    }

    /// <summary>
    /// 死んでいるいる状態
    /// </summary>
    public class CharacterStateDead : CharacterState
    {
        public override string GetStateName()
        {
            return "State:Dead";
        }
        public override void Update(EnemyManager em)
        {
            em.DeadUpdate();
        }
        public override void Execute()
        {
            if (ExecAction != null) ExecAction();
        }
    }
}