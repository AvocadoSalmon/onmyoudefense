using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//*****************************************************************
//
//  召喚するコスト（霊力）を管理するクラス
//
//*****************************************************************
public class BaseSummonPower : MonoBehaviour
{
    [SerializeField] private Image _PowerBar;                       //  霊力バー
    [SerializeField] private Image _VirualPowerBar;                 //  仮想霊力バー
    [SerializeField] private Image _CostBar;                        //  召喚コストバー
    [SerializeField] private Text _PowerText;                       //  霊力値テキスト
    //  マウスオーバーイベント
    [SerializeField] private EventTrigger _TsuruTrigger;
    [SerializeField] private EventTrigger _NezumiTrigger;
    [SerializeField] private EventTrigger _SaruTrigger;
    [SerializeField] private EventTrigger _InuTrigger;
    [SerializeField] private EventTrigger _ToraTrigger;

    //  霊力がたまるSE
    [SerializeField] private AudioClip _KeyStoneChargeClip = default;

    private float _PowerValue;                      //  霊力値
    private float _PowerValueMax = 100f;            //  霊力最大値
    private float _VirualPowerValue;                //  仮想霊力値
    private float _VirualPowerIdealValue;           //  理想仮想霊力値
    private float _FillSpeed = 200.0f;              //  伸びるスピード
    private bool _IsMousseOver;                     //  マウスオーバーフラグ
    private float _PowerStackInterval = 3f;         //  霊力が溜まる間隔
    private float _PowerStackAmount = 15f;          //  霊力が溜まる量
    private Coroutine _StackPowerCoroutine;         //  霊力が溜まるコルーチン
    private AudioSource[] _AudioSource;             //  AudioSource

    //  マウスオーバーイベントのタイプ
    private enum EventType
    {
        IN,
        OUT,

        MAX
    };

    public PlayerData[] _playerDataList;   //  パラメータリスト

    // Start is called before the first frame update
    void Start()
    {
        _PowerValue = 5;
        _CostBar.fillAmount = 0;
        _VirualPowerValue = 0;
        _VirualPowerIdealValue = 0;
        _IsMousseOver = false;

        //  霊力ゲージ以外を無効化する
        _VirualPowerBar.enabled = false;
        _CostBar.enabled = false;

        //  各キャラのパラメータ設定
        _playerDataList = new PlayerData[(int)GameManager.PlayerType.MAX]
        {
            new PlayerData((int)GameManager.PlayerType.TSURU ), // TSURU,
            new PlayerData((int)GameManager.PlayerType.NEZUMI), // NEZUMI,
            new PlayerData((int)GameManager.PlayerType.SARU),   // SARU,
            new PlayerData((int)GameManager.PlayerType.INU),    // INU,
            new PlayerData((int)GameManager.PlayerType.TORA)    // TORA,
        };

        // マウスオーバーイベント
        var entry1 = new EventTrigger.Entry();
        var entry2 = new EventTrigger.Entry();
        var entry3 = new EventTrigger.Entry();
        var entry4 = new EventTrigger.Entry();
        var entry5 = new EventTrigger.Entry();
        entry1.eventID = EventTriggerType.PointerEnter;
        entry2.eventID = EventTriggerType.PointerEnter;
        entry3.eventID = EventTriggerType.PointerEnter;
        entry4.eventID = EventTriggerType.PointerEnter;
        entry5.eventID = EventTriggerType.PointerEnter;
        // マウスオーバーが外れるイベント
        var away1 = new EventTrigger.Entry();
        var away2 = new EventTrigger.Entry();
        var away3 = new EventTrigger.Entry();
        var away4 = new EventTrigger.Entry();
        var away5 = new EventTrigger.Entry();
        away1.eventID = EventTriggerType.PointerExit;
        away2.eventID = EventTriggerType.PointerExit;
        away3.eventID = EventTriggerType.PointerExit;
        away4.eventID = EventTriggerType.PointerExit;
        away5.eventID = EventTriggerType.PointerExit;

        // イベント登録
        entry1.callback.AddListener((data) => 
        { StartCoroutine(SetCostToIdealOver(_playerDataList[(int)GameManager.PlayerType.TSURU]._Cost)); });
        entry2.callback.AddListener((data) => 
        { StartCoroutine(SetCostToIdealOver(_playerDataList[(int)GameManager.PlayerType.NEZUMI]._Cost)); });
        entry3.callback.AddListener((data) => 
        { StartCoroutine(SetCostToIdealOver(_playerDataList[(int)GameManager.PlayerType.SARU]._Cost)); });
        entry4.callback.AddListener((data) => 
        { StartCoroutine(SetCostToIdealOver(_playerDataList[(int)GameManager.PlayerType.INU]._Cost)); });
        entry5.callback.AddListener((data) => 
        { StartCoroutine(SetCostToIdealOver(_playerDataList[(int)GameManager.PlayerType.TORA]._Cost)); });
        _TsuruTrigger.triggers.Add(entry1);
        _NezumiTrigger.triggers.Add(entry2);
        _SaruTrigger.triggers.Add(entry3);
        _InuTrigger.triggers.Add(entry4);
        _ToraTrigger.triggers.Add(entry5);

        away1.callback.AddListener((data) => { StartCoroutine(SetCostToIdealAway()); });
        away2.callback.AddListener((data) => { StartCoroutine(SetCostToIdealAway()); });
        away3.callback.AddListener((data) => { StartCoroutine(SetCostToIdealAway()); });
        away4.callback.AddListener((data) => { StartCoroutine(SetCostToIdealAway()); });
        away5.callback.AddListener((data) => { StartCoroutine(SetCostToIdealAway()); });
        _TsuruTrigger.triggers.Add(away1);
        _NezumiTrigger.triggers.Add(away2);
        _SaruTrigger.triggers.Add(away3);
        _InuTrigger.triggers.Add(away4);
        _ToraTrigger.triggers.Add(away5);

        //  オーディオソースを取得
        _AudioSource = GameObject.Find("GameManager").GetComponents<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _PowerValue++;
        }
        if (Input.GetKey(KeyCode.S))
        {
            _PowerValue--;
        }

        UpdatePowerValueAndText();


    }

    //------------------------------------------------------------------------------
    //  コスト値を理想コスト値に近づける(マウスオーバー)
    //------------------------------------------------------------------------------
    private IEnumerator SetCostToIdealOver(float value)
    {
        //  マウスオーバーフラグTrue
        _IsMousseOver = true;

        _VirualPowerBar.enabled = true; //  マウスオーバーで有効化する
        _CostBar.enabled = true;        //  マウスオーバーで有効化する

        //  理想値を保存
        _VirualPowerIdealValue = _PowerValue - value;

        //  理想値まで増やす
        while (_VirualPowerValue < _VirualPowerIdealValue)
        {
            _VirualPowerValue += _FillSpeed * Time.deltaTime;

            yield return null;
        }

        //  超えたら理想値に設定する
        if (_VirualPowerValue >= _VirualPowerIdealValue)
        {
            _VirualPowerValue = _VirualPowerIdealValue;
        }

        _VirualPowerBar.fillAmount = _VirualPowerValue / _PowerValueMax;   //  0.0〜1.0にする
    }

    //------------------------------------------------------------------------------
    //  コスト値を理想コスト値に近づける（マウスが離れる時）
    //------------------------------------------------------------------------------
    private IEnumerator SetCostToIdealAway()
    {
        //  マウスオーバーフラグfalse
        _IsMousseOver = false;

        //  理想値を保存
        _VirualPowerIdealValue = _PowerValue;

        //  理想値まで減らす
        while (_VirualPowerValue > _VirualPowerIdealValue)
        {
            _VirualPowerValue -= _FillSpeed * Time.deltaTime;

            yield return null;
        }

        //  超えたら理想値に設定する
        if (_VirualPowerValue <= _VirualPowerIdealValue)
        {
            _VirualPowerValue = _VirualPowerIdealValue;
        }

        _VirualPowerBar.fillAmount = _VirualPowerValue / _PowerValueMax;   //  0.0〜1.0にする

        //  マウスオーバーで無効化する
        _VirualPowerBar.enabled = false;
        _CostBar.enabled = false;

        yield return null;

    }

    //------------------------------------------------------------------------------
    //  霊力ゲージとテキストを更新する
    //------------------------------------------------------------------------------
    private void UpdatePowerValueAndText()
    {
        //  値を制限
        if (_PowerValue > _PowerValueMax)
        {
            _PowerValue = _PowerValueMax;
        }
        else if(_PowerValue < 0)
        {
            _PowerValue = 0;
        }

        //  更新
        _PowerBar.fillAmount = _PowerValue  / _PowerValueMax;
        _CostBar.fillAmount = _PowerValue / _PowerValueMax;
        _VirualPowerBar.fillAmount = _VirualPowerValue / _PowerValueMax;
        _PowerText.text = _PowerValue.ToString();

    }

    //------------------------------------------------------------------------------
    //  ボタンが押された時の処理
    //------------------------------------------------------------------------------
    public void OnClickSummonButton(int type)
    {
        //  仮想霊力値の理想値を更新しておく
        StartCoroutine(SetCostToIdealOver(_playerDataList[type]._Cost));

        //  霊力値を仮想霊力値の理想値に設定
        _PowerValue = _VirualPowerIdealValue;

    }

    //------------------------------------------------------------------------------
    //  現在の霊力を取得する
    //------------------------------------------------------------------------------
    public float GetPowerValue()
    {
        return _PowerValue;
    }

    //------------------------------------------------------------------------------
    //  指定秒に一回、指定量霊力が増える
    //------------------------------------------------------------------------------
    private IEnumerator StackPowerByInterval(float interval, float power)
    {
        while (true)
        {
            yield return new WaitForSeconds(interval);

            if (_PowerValue < _PowerValueMax)
            {
                _PowerValue += power;

                //  霊力がたまるSEを再生
                _AudioSource[(int)GameManager.AudioType.SFX_KEYSTONE_CHARGE].clip = _KeyStoneChargeClip;
                _AudioSource[(int)GameManager.AudioType.SFX_KEYSTONE_CHARGE].Play();
            }
            else
            {
                _PowerValue = _PowerValueMax;
            }


        }
    }

    //------------------------------------------------------------------------------
    //  霊力チャージを開始する
    //------------------------------------------------------------------------------
    public void StartStackPower()
    {
        //  霊力チャージ開始！
        _StackPowerCoroutine = StartCoroutine(StackPowerByInterval(_PowerStackInterval, _PowerStackAmount));
    }
    //------------------------------------------------------------------------------
    //  霊力チャージを止める
    //------------------------------------------------------------------------------
    public void StopStackPower()
    {
        StopCoroutine(_StackPowerCoroutine);
    }

}
