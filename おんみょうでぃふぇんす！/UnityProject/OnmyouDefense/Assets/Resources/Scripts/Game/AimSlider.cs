using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//*****************************************************************
//
//  召喚レーンのスライダークラス
//
//*****************************************************************
public class AimSlider : MonoBehaviour
{
    // eventSystemを取得するための変数宣言
    [SerializeField] EventSystem eventSystem;
    //  スライダーボタン画像のButtonコンポーネント
    [SerializeField] private  GameObject _SliderButton = default;
    //  他のAimSlider
    [SerializeField] private AimSlider[] _OtherSliders = default;
    //  マウスオーバーイベント
    [SerializeField] private EventTrigger _SliderTrigger;
    //  レーンオーバーレイ時SE
    [SerializeField] private AudioClip _LaneOverRaySeClip = default;
    //  レーン選択時時SE
    [SerializeField] private AudioClip _LaneSelectSeClip = default;


    //  スライダーのSliderコンポーネント
    private Slider _Slider;
    //  スライダーの初期値
    private float _FirstValue = 5f;
    //  スライダーが伸びるスピード
    private float _ExtendSpeed = 0.15f;
    //  選択されいるかどうかのフラグ
    private bool _IsSelected;
    //  AudioSource
    private AudioSource[] _AudioSource;

    void Start()
    {
        //  コンポーネントを取得
        _Slider = this.GetComponent<Slider>();

        //  AudioSourceを取得
        _AudioSource = GameObject.Find("GameManager").GetComponents<AudioSource>();

        //  初期値を設定
        _Slider.value = _FirstValue;
        _IsSelected = false;

        // マウスオーバーイベント
        var entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;

        // マウスオーバーが外れるイベント
        var away = new EventTrigger.Entry();
        away.eventID = EventTriggerType.PointerExit;

        // イベント登録
        entry.callback.AddListener((data) =>
        {
            _AudioSource[(int)GameManager.AudioType.SFX_MOUSE].clip = _LaneOverRaySeClip;
            _AudioSource[(int)GameManager.AudioType.SFX_MOUSE].Play();
        });
        _SliderTrigger.triggers.Add(entry);

        away.callback.AddListener((data) => { });
        _SliderTrigger.triggers.Add(away);
    }

    void Update()
    {
        //  このボタンが選択されている間はスライダーを更新
        if (_IsSelected)
        {
            _Slider.value += _ExtendSpeed;
            if (_Slider.value >= _Slider.maxValue) _Slider.value = _FirstValue;
        }
        else _Slider.value = _FirstValue;
    }

    //-------------------------------------------------------------------------------
    //  _IsSelectedのプロパティ
    //-------------------------------------------------------------------------------
    public bool IsSelected()
    {
        return _IsSelected;
    }

    public void SetIsSelected(bool selected)
    {
        if (_IsSelected != selected)
        {
            _IsSelected = selected;

            Debug.Log("_IsSelectedが " + _IsSelected + "にセットされました");
        }
    }

    //-------------------------------------------------------------------------------
    //  ボタンが押された時の処理
    //-------------------------------------------------------------------------------
    public void OnClickSlider()
    {
        _IsSelected = true;  // 選択された！

        //  押されたボタン以外のボタンはFalseにする
        for (int i = 0; i < (int)GameManager.LaneType.MAX - 1; i++)
        {
            _OtherSliders[i].SetIsSelected(false);
        }

        //  選択時SEを再生
        _AudioSource[(int)GameManager.AudioType.SFX_MOUSE].clip = _LaneSelectSeClip;
        _AudioSource[(int)GameManager.AudioType.SFX_MOUSE].Play();

    }

    //-------------------------------------------------------------------------------
    //  ボタンの状態を設定
    //-------------------------------------------------------------------------------
    public void SetButtonActive(bool active)
    {
        this.enabled = active;
        _SliderButton.GetComponent<Button>().enabled = active;
    }
}
