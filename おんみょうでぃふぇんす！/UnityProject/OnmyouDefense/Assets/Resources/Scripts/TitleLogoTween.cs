using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TitleLogoTween : MonoBehaviour
{
    //  TweenSE
    [SerializeField] private AudioClip _TweenClip = default;
    //  ロゴの背景の光
    [SerializeField] private GameObject _LogoBack = default;

    //  オーディオソース
    private AudioSource _AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        //  コンポーネントを取得
        _AudioSource = GetComponent<AudioSource>();

    }

    //--------------------------------------------------
    //  アニメーション再生
    //--------------------------------------------------
    public IEnumerator StartAnimation()
    {
        bool complete = false;

        this.enabled = true;

        Sequence sequence = DOTween.Sequence();

        transform.localScale = new Vector3(0f, 0f, 0f);
        sequence.Append
            (
                transform.DOLocalRotate(new Vector3(0, 0, -360f * 7), 0.5f, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Restart)
            );
        sequence.Join
            (
                transform.DOScale(1f, 0.5f)
                .OnComplete(() =>
                {
                    //  SEを再生
                    _AudioSource.clip = _TweenClip;
                    _AudioSource.Play();

                    //  ロゴ背景有効化
                    _LogoBack.SetActive(true);

                    complete = true;
                })
            );


        //  完了まで待つ
        yield return new WaitUntil(() => complete == true);

        //  １秒待つ
        yield return new WaitForSeconds(1.0f);
    }
}
