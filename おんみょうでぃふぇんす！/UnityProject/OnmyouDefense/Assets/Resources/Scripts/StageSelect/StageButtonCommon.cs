using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//*****************************************************************
//
//  Stageボタン基底クラス
//
//*****************************************************************
public class StageButtonCommon : MonoBehaviour
{
    [SerializeField] private GameObject _CheckMarkObj = default;
    [SerializeField] private GameObject _QuestionObj = default;
    [SerializeField] private AudioClip _SelectClip = default;
    [SerializeField] private AudioClip _SelectOverClip = default;    
    [SerializeField] private AudioSource _AudioSource = default;

    protected ButtonScaling _buttonScaling;
    protected EventTrigger _Trigger;
    protected int _StageID;
    protected bool _ClearFlag;
    protected bool _QuestionFlag;
    protected bool _OnClickFlag;
    //ロードするステージのファイルパス
    protected string _LoadStagePath;

    public virtual void Start()
    {
        _StageID = (int)GameManager.StageID.STAGE_01;
        _LoadStagePath = "";
        _ClearFlag = false;
        _OnClickFlag = false;

        //  コンポーネントを設定
        _buttonScaling = this.GetComponent<ButtonScaling>();
        _Trigger = this.GetComponent<EventTrigger>();

        // マウスオーバーイベント
        var entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        // マウスオーバーが外れるイベント
        var away = new EventTrigger.Entry();
        away.eventID = EventTriggerType.PointerExit;

        // イベント登録
        entry.callback.AddListener((data) => 
        {
            //  マウスオーバーでSEを再生
            _AudioSource.clip = _SelectOverClip;
            _AudioSource.Play();

            _buttonScaling.OnOver(); 
        });
        _Trigger.triggers.Add(entry);

        away.callback.AddListener((data) => { _buttonScaling.OnAway(); });
        _Trigger.triggers.Add(away);   
    }


    public virtual void Update()
    {
        UpdateClearMark();
        UpdateQuestionMark();

        //  クリア済みか?マーク表示中はマウスに反応しない
        if(_ClearFlag || _QuestionFlag || _OnClickFlag)
        {
            this.GetComponent<Image>().raycastTarget = false;
            this.GetComponent<Button>().interactable = false;
        }
        else
        {
            this.GetComponent<Image>().raycastTarget = true;
            this.GetComponent<Button>().interactable = true;
        }
    }

    //------------------------------------------------------
    //  _StageIDのプロパティ
    //------------------------------------------------------
    public virtual void SetStageID(int id)
    {
        _StageID = id;
    }

    public virtual int GetStageID()
    {
        return _StageID;
    }

    //------------------------------------------------------
    //  ステージ選択された時の処理
    //------------------------------------------------------
    public virtual void OnClick()
    {
        if(!_OnClickFlag)
        {
            _OnClickFlag = true;

            //選択音を再生
            _AudioSource.clip = _SelectClip;
            _AudioSource.Play();

            //自分のステージIDをマネージャーに渡す
            StageIdManager.Instance.SetStageID(_StageID);

            //自分のプレハブのパスをマネージャーに渡す
            StageIdManager.Instance.SetLoadStagePath(_LoadStagePath);

            //ゲーム画面へ
            ChangeSceneToGame();
        }
    }

    //------------------------------------------------------
    //  クリアマークの設定
    //------------------------------------------------------
    public virtual void UpdateClearMark()
    {
        if(_ClearFlag)_CheckMarkObj.SetActive(true);
        else _CheckMarkObj.SetActive(false);
    }
    
    //------------------------------------------------------
    //  クリアマークのプロパティ
    //------------------------------------------------------
    public virtual void SetClearFlag(bool clearFlag)
    {
        _ClearFlag = clearFlag;
    }

    public virtual bool GetClearFlag()
    {
        return _ClearFlag;
    }

    //------------------------------------------------------
    //  クエスチョンマークの設定
    //------------------------------------------------------
    public virtual void UpdateQuestionMark()
    {
        if(_QuestionFlag)_QuestionObj.SetActive(true);
        else _QuestionObj.SetActive(false);
    }
    
    //------------------------------------------------------
    //  クエスチョンマークのプロパティ
    //------------------------------------------------------
    public virtual void SetQuestionFlag(bool questionFlag)
    {
        _QuestionFlag = questionFlag;
    }

    public virtual bool GetQuestionFlag()
    {
        return _QuestionFlag;
    }

    //----------------------------------------------------------------------------------
    //  ゲーム画面に遷移する
    //----------------------------------------------------------------------------------
    public void ChangeSceneToGame()
    {
        //  初回フラグ設定
        bool isFirst = PlayerPrefsBool.GetBool("isPlayingFirst",false);

        if (_StageID == (int)GameManager.StageID.STAGE_01)  //  ステージ１で
        {
            if (true == isFirst) //初回プレイフラグがONなら初回
            {
                //初回フラグONしてSAVE
                PlayerPrefsBool.SetBool("isPlayingFirst", true);
                PlayerPrefs.Save();
            }
        }

            //  ゲームシーンへ移行
            ChangeScene.Execute("SceneGame");
    }
}
