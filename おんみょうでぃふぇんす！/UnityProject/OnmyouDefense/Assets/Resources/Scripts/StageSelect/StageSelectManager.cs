using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//*****************************************************************
//
//  SceneStageSelect
//
//*****************************************************************
public class StageSelectManager : MonoBehaviour
{
    [SerializeField] List<StageButtonCommon> _StageList = default;
    [SerializeField] Text _UserNameText = default;
    [SerializeField] Text _StageCleatNumText = default;

    private static string _PlayerName = "名もなき陰陽師";  //プレイヤーの名前
    private static int _StageClearNum = 0;                 //プレイヤーのステージクリア数
    

    void Start()
    {

    }

    
    void Update()
    {
        if(_StageClearNum > _StageList.Count)
            _StageClearNum = 0;

        UpdateStageClearState();
        UpdateStageQuestionState();

        _UserNameText.text = 
            "ユーザーネーム：" + _PlayerName;
        _StageCleatNumText.text = 
            "ステージクリア数：" + _StageClearNum.ToString();


        //デバッグ表示
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Space))
        {
            _StageClearNum++;

            if(_StageClearNum > _StageList.Count)
            _StageClearNum = 0;

            Debug.Log(_StageClearNum);
        }
#endif

        
    }

    //----------------------------------------------------------------------------------
    //  _StageClearNumのプロパティ関数
    //----------------------------------------------------------------------------------
    public static int GetStageClearNum(){

		return _StageClearNum;
	}

    public static void SetStageClearNum(int num)
    {
        _StageClearNum = num;
    }

    //----------------------------------------------------------------------------------
    //  _PlayerNameのプロパティ関数
    //----------------------------------------------------------------------------------
    public static string GetPlayerName(){

		return _PlayerName;
	}

    public static void SetPlayerName(string name)
    {
        _PlayerName = name;
    }
    
    //----------------------------------------------------------------------------------
    //  ステージのクリア表示状態をセットする
    //----------------------------------------------------------------------------------
    private void UpdateStageClearState()
    {
        //クリア数によってステージにクリアフラグを設定
        if( _StageClearNum == 0 )   //全部非表示
        {
            for( int i=0;i<_StageList.Count;i++ )
            {
                _StageList[i].SetClearFlag(false);
            }
        }
        else if( _StageClearNum > 0 ){   //クリア数1以上

            for( int i=0;i<=_StageClearNum;i++ )
            {
                if(i == 0)continue;
                _StageList[i-1].SetClearFlag(true);
            }
        }
    }

    //----------------------------------------------------------------------------------
    //  ステージの？マーク表示状態をセットする
    //----------------------------------------------------------------------------------
    private void UpdateStageQuestionState()
    {
        //クリア数が（ステージ数-1）以上は全て非表示
        if (_StageClearNum >= _StageList.Count-1)
        {   
            for (int i=0;i<_StageList.Count;i++)
            {
                _StageList[i].SetQuestionFlag(false);
            }
        }
        //クリア数が0以上で(ステージ数-1)より小さい時は表示
        else if( _StageClearNum >= 0 )
        {   
            //  表示設定
            for (int i=_StageList.Count-1;i>=_StageClearNum+1;i--)
            {
                _StageList[i].SetQuestionFlag(true);
            }

            //非表示設定
            for (int i=0;i<=_StageClearNum;i++)
            {
                _StageList[i].SetQuestionFlag(false);
            }
        }
    }
    //----------------------------------------------------------------------------------
    //  ゲーム終了ボタン
    //----------------------------------------------------------------------------------
    public void OnClickGameQuitButton()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_STANDALONE
            UnityEngine.Application.Quit();
        #endif
    }
}
