using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//*****************************************************************
//
//  Stage01ボタンクラス
//
//*****************************************************************
public class Stage03Button : StageButtonCommon
{
    public override void Start()
    {
        base.Start();

        _StageID = (int)(GameManager.StageID.STAGE_03);
        _LoadStagePath = "Prefabs/Stage/Stage03";
    }
}
