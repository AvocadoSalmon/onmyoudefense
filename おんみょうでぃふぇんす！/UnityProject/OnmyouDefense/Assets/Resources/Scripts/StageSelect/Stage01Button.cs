using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//*****************************************************************
//
//  Stage01ボタンクラス
//
//*****************************************************************
public class Stage01Button : StageButtonCommon
{
    public override void Start()
    {
        base.Start();

        //ステージ個別設定
        _StageID = (int)(GameManager.StageID.STAGE_01);
        _LoadStagePath = "Prefabs/Stage/Stage01";
    }
}
