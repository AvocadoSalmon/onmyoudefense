using UnityEngine;
using DG.Tweening;

public class Popup : MonoBehaviour {

    private bool _CustomFlag = false;

    private void Start() {
        if (!_CustomFlag)   //  カスタム引数が設定されていない時
        {
            transform.localScale = new Vector3(0f, 0f, 0f);
            transform.DOScale(1f, 0.2f);
        }
    }

    public void OnOpen() {
        transform.localScale = new Vector3(0f,0f,0f);
        transform.DOScale(1f,0.2f);
    }

    public void OnOpen(float time)
    {
        transform.localScale = new Vector3(0f, 0f, 0f);
        transform.DOScale(1f, time);
    }

    public void OnClose() {
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(0f, 0.2f));
        seq.OnComplete(() => DestroyWindow());
        seq.Play();
    }

    public void OnClose(float time)
    {
        DestroyWindow();
    }

    void DestroyWindow() {
        Destroy(gameObject);
    }

    public void SetCustomFlag(bool flag)
    {
        _CustomFlag = flag;
    }
}
