<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Onmyou $onmyou
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Onmyou'), ['action' => 'edit', $onmyou->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Onmyou'), ['action' => 'delete', $onmyou->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $onmyou->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Onmyou'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Onmyou'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="onmyou view large-9 medium-8 columns content">
    <h3><?= h($onmyou->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($onmyou->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($onmyou->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ClearNum') ?></th>
            <td><?= $this->Number->format($onmyou->ClearNum) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($onmyou->Date) ?></td>
        </tr>
    </table>
</div>
