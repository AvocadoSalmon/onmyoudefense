<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Onmyou $onmyou
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $onmyou->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $onmyou->Id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Onmyou'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="onmyou form large-9 medium-8 columns content">
    <?= $this->Form->create($onmyou) ?>
    <fieldset>
        <legend><?= __('Edit Onmyou') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('ClearNum');
            echo $this->Form->control('Date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
