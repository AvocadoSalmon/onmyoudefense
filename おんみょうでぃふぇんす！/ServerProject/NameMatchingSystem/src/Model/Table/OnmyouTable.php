<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Onmyou Model
 *
 * @method \App\Model\Entity\Onmyou get($primaryKey, $options = [])
 * @method \App\Model\Entity\Onmyou newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Onmyou[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Onmyou|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Onmyou|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Onmyou patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Onmyou[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Onmyou findOrCreate($search, callable $callback = null, $options = [])
 */
class OnmyouTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('onmyou');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmpty('Id', 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmpty('Name');

        $validator
            ->integer('ClearNum')
            ->requirePresence('ClearNum', 'create')
            ->notEmpty('ClearNum');

        $validator
            ->dateTime('Date')
            ->requirePresence('Date', 'create')
            ->notEmpty('Date');

        return $validator;
    }
}
