<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Onmyou Controller
 *
 * @property \App\Model\Table\OnmyouTable $Onmyou
 *
 * @method \App\Model\Entity\Onmyou[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OnmyouController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $onmyou = $this->paginate($this->Onmyou);

        $this->set(compact('onmyou'));
    }

    /**
     * View method
     *
     * @param string|null $id Onmyou id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $onmyou = $this->Onmyou->get($id, [
            'contain' => []
        ]);

        $this->set('onmyou', $onmyou);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $onmyou = $this->Onmyou->newEntity();
        if ($this->request->is('post')) {
            $onmyou = $this->Onmyou->patchEntity($onmyou, $this->request->getData());
            if ($this->Onmyou->save($onmyou)) {
                $this->Flash->success(__('The onmyou has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The onmyou could not be saved. Please, try again.'));
        }
        $this->set(compact('onmyou'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Onmyou id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $onmyou = $this->Onmyou->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $onmyou = $this->Onmyou->patchEntity($onmyou, $this->request->getData());
            if ($this->Onmyou->save($onmyou)) {
                $this->Flash->success(__('The onmyou has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The onmyou could not be saved. Please, try again.'));
        }
        $this->set(compact('onmyou'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Onmyou id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $onmyou = $this->Onmyou->get($id);
        if ($this->Onmyou->delete($onmyou)) {
            $this->Flash->success(__('The onmyou has been deleted.'));
        } else {
            $this->Flash->error(__('The onmyou could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //----------------------------------------------------
    //  メッセージの取得
    //----------------------------------------------------
    public function getMessage(){
		error_log("getMessage()");
		//---------------
		// Viewのレンダーを無効化
		// これを行う事で対になるテンプレート(.tpl)が不要となる。
		$this->autoRender = false;

		// DBからデータを読み込んで配列に変換
		//[Messageboard]テーブルからクエリを取得
		$query = $this->Onmyou->find('all');

		//クエリを実行してarrayにデータを格納
		$json_array = json_encode($query);

		//---------------
		// $json_array の内容を出力
		echo $json_array;
	}

    //----------------------------------------------------
    //  メッセージをDBへセット
    //----------------------------------------------------
    public function setMessage()
    {
        error_log("setMessage()");
        //---------------
        // Viewのレンダーを無効化
        // これを行う事で対になるテンプレート(.tpl)が不要となる。
        $this->autoRender = false;

        // Onmyouテーブルのid:1取得
        $data = $this->Onmyou->get('1');

        // postデータ
        $post_data = $this->request->getData();

        // ユーザーデータをフォームからのpostデータで上書き
        $data = $this->Onmyou->patchEntity($data, $post_data);

        if( $this->Onmyou->save($data) )
        {
            echo "success";	//success!
        }else{
            echo "failed";	//failed!
        }
	}

    //----------------------------------------------------
    //  メッセージを新しいレコードとして追加
    //----------------------------------------------------
    public function createMessage()
    {
        error_log("createMessage()");
        //---------------
        // Viewのレンダーを無効化
        // これを行う事で対になるテンプレート(.tpl)が不要となる。
        $this->autoRender = false;

		// name,message をPOSTで受け取る。
	    $name	= $this->request->getData("Name");
		$num	= $this->request->getData("ClearNum");

        //テーブルに追加するレコードを作る
        $record = array(
            "Name"=>$name,
            "ClearNum"=>$num,
            "Date"=>date("Y/m/d H:i:s")
        );

        //  新規データとして設定
        $entity = $this->Onmyou->newEntity();
		$entity = $this->Onmyou->patchEntity($entity, $record);

        if( $this->Onmyou->save($entity) )
        {
            echo "success";	//success!
        }else{
            echo "failed";	//failed!
        }
	}

    //----------------------------------------------------
    //  全レコードを消去
    //----------------------------------------------------
    public function deleteAll()
    {
        $this->Onmyou->deleteAll([]);

        return $this->redirect(['action' => 'index']);
    }

    //----------------------------------------------------
    //  レコードが存在するかどうか :存在すればtrue
    //----------------------------------------------------
    public function getIsExists()
    {
        error_log("getIsExists()");

        //---------------
        // Viewのレンダーを無効化
        // これを行う事で対になるテンプレート(.tpl)が不要となる。
        $this->autoRender = false;

	    $id	= $this->request->getData("Id");

        $status = '1';
        $count = $this->Onmyou->find()->where(['Id' => $id])->count();

		//クエリを実行してarrayにデータを格納
		$json = json_encode($count);

    }
}
