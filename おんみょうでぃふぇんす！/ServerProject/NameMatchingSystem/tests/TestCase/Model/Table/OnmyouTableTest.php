<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OnmyouTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OnmyouTable Test Case
 */
class OnmyouTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OnmyouTable
     */
    public $Onmyou;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.onmyou'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Onmyou') ? [] : ['className' => OnmyouTable::class];
        $this->Onmyou = TableRegistry::getTableLocator()->get('Onmyou', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Onmyou);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
